import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
// import Woman from '../assets/contact-us/woman.png';
// import Background from '../assets/contact-us/backgroud.png';
// import Rectangle from '../assets/contact-us/rectangle.png';
import Cover from '../assets/contact-us/cover.png';
import './styles/contact.css';
import {Link , Route} from 'react-router-dom';
import Navbar from '../components/CustomNavbar';
// import Image from '../assets/contact-us/img1.png';
// import Address from '../assets/contact-us/address.png';
// import Phone from '../assets/contact-us/phone.png';
// import Mail from '../assets/contact-us/mail.png';


export default class contact extends Component{
    render(){
        return (  
            <Grid container>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                <div  className="cover-size">
                    <Navbar />
                </div>
                </Grid>
                <Grid container>
                    <Grid  item xs={12} sm={12} md={12} lg={12}>
                        <div  className="background1-size">
                        <div className='background2-size'></div>
                        <Grid container>
                            <Grid item xs={2} sm={2} md={2} lg={2}></Grid>
                            <Grid item xs={0} sm={0} md={0} lg={0}></Grid>
                            <Grid item xs={1} sm={1} md={1} lg={1} style={{marginLeft:'-15px'}}>
                                <div className='address-logo' ></div>
                            </Grid>
                            <Grid item xs={1} sm={1} md={1} lg={1}></Grid>
                            <Grid item xs={2} sm={2} md={2} lg={2}></Grid>
                            <Grid item xs={2} sm={1} md={1} lg={1}>
                                <div className='phone-logo'></div>
                            </Grid>

                            <Grid item xs={1} sm={2} md={2} lg={1}></Grid>
                            <Grid item xs={1} sm={1} md={1} lg={2}></Grid>
                            <Grid item xs={2} sm={1} md={1} lg={1}>
                                <div className='email-logo'></div>
                                
                            </Grid>
                            
                        </Grid>
                        <Grid container>
                            <Grid item xs={1} sm={0} md={0} lg={0}></Grid>
                            <Grid item xs={0} sm={0} md={0} lg={0}></Grid>
                            <Grid item xs={2} sm={2} md={2} lg={2}>
                            <p className='font'>Address</p>
                            <p className='font2'>282 Horton Road Datchet SL39HN (UK)47096</p>
                            </Grid>
                            <Grid item xs={0} sm={0} md={1} lg={1}></Grid>
                            <Grid item xs={1} sm={2} md={1} lg={1}></Grid>
                            <Grid item xs={3} sm={2} md={2} lg={2} style={{marginLeft:'15px'}}>
                            <p className='font'>Phone</p>
                            <p className='font2'>(0044) 7968 670</p>
                            </Grid>
                            <Grid item xs={0} sm={1} md={1} lg={1}></Grid>
                            <Grid item xs={1} sm={1} md={1} lg={1}></Grid>
                            <Grid item xs={3} sm={2} md={2} lg={2}>
                            <p className='font' style={{marginLeft:'-15px'}}>Email</p>
                            <p className='font2'>info@rndtek.co.uk.com</p>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item lg={6} xs={6}>
                            <div className='contact-box'>
                            <textarea></textarea>
                            </div>
                        </Grid>
                        <Grid item lg={4} xs={4}>
                            <div className='contact-image'></div>
                        </Grid>
                        <Grid item lg={12}>
                            <div className='contact-woman'></div>
                        </Grid>
                        </Grid>
                        </div>
                    </Grid>
                </Grid>

                
            </Grid>
            
            
          );
    }
}






{/* <div>
              <img src={Cover} alt='cover' style={{width:'100%', height:'300px'}}></img>;
              <img src={Rectangle} alt='rectangle' style={{marginTop:'-25px'}}></img>
              <img src={Background} alt='background' style={{position:'absolute', marginTop:'-1500px'}}></img>
              <div style={{color:'white', height:'400px', width:'800px', backgroundColor:'#0071bc', marginTop:'-800px',marginLeft:'0px', position:'absolute'}}>
              <h1 style={{marginLeft:'200px'}}>Get in touch</h1>
              <p style={{marginLeft:'200px'}}>Contact us</p>
              <textarea style={{marginLeft:'200px'}}></textarea><textarea style={{marginLeft:'10px'}}></textarea> <br />
              <textarea style={{height:'100px', marginLeft:'200px' , width:'330px' , marginTop:'20px'}}></textarea>
              </div>
              <div style={{height:'350px',width:'400px',position:'absolute', marginTop:'-750px', marginLeft:'700px'}}>
              <img src={Image} alt='image' style={{position:'absolute', marginTop:'0px', height:'300px',width:'400px'}}></img>
              </div>
              <img src={Woman} alt='image' style={{position:'absolute', marginTop:'-750px', height:'300px',width:'250px', marginLeft:'650px'}}></img>
              <img src={Address} style={{position:'absolute', marginTop:'-920px', marginLeft:'320px'}}></img>
              <p style={{position:'absolute', marginTop:'-870px', marginLeft:'300px', color:'white'}}>Address</p>
              <p style={{position:'absolute', marginTop:'-840px', marginLeft:'220px', color:'white'}}>282 Horton Road Datchet SL39HN</p>
              <img src={Phone} style={{position:'absolute', marginTop:'-920px', marginLeft:'650px'}}></img>
              <p style={{position:'absolute', marginTop:'-870px', marginLeft:'650px', color:'white'}}>Phone</p>
              <p style={{position:'absolute', marginTop:'-840px', marginLeft:'620px', color:'white'}}>(0044) 7968 670</p>
              <img src={Mail} style={{position:'absolute', marginTop:'-920px', marginLeft:'1000px'}}></img>
              <p style={{position:'absolute', marginTop:'-870px', marginLeft:'1000px', color:'white'}}>Email</p>
              <p style={{position:'absolute', marginTop:'-840px', marginLeft:'930px', color:'white'}}>info@rndtek.co.ukmail.com</p>
              <Header changeTitle={this.changeTitle.bind(this)} title={this.state.title} />
              
            </div> */}