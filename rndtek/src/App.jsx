import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Route} from 'react-router-dom';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          
          <Route exact={true} path='/' render={() => (
            <div className="App">
              <Home />

            </div>
          )}/>
          <Route exact={true} path='/about' render={() => (
            <div className="App">
              <About />
            </div>
          )}/>
          <Route exact={true} path='/contact' render={() => (
            <div className="App">
              <Contact />
            </div>
          )}/>
        </div>
      </BrowserRouter>
    );
  }
}
export default App;