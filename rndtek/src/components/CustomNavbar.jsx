import React, {Component} from 'react'
import {Navbar, Nav, NavItem ,Image,Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './CustomNavbar.css';
import  logo from'../assets/logo.png';
import callicon from '../assets/call sign.png';

export default class CustomNavbar extends Component{
    render(){
        return(
            <Navbar default collapseOnSelect>
             <Navbar.Header>
              <div className="logoimg">
              <Image src={logo} style={{width:137, marginLeft:50}} />

              </div>
                  <Navbar.Brand>
                  
                    <h2></h2>
                  </Navbar.Brand>
                  <Navbar.Toggle />
                  </Navbar.Header>
              
              <Navbar.Collapse>
                  <Nav pullRight>
                     <NavItem eventKey={1} componentClass={Link} href="/" to="/">
                      Home
                     </NavItem>
                     <NavItem eventKey={2} componentClass={Link} href="/ourservices" to="/ourservices">
                      Our Servies
                     </NavItem>
                     <NavItem eventKey={2} componentClass={Link} href="/portfolio" to="/portfolio">
                      Portfolio
                     </NavItem>
                     <NavItem eventKey={2} componentClass={Link} href="/about" to="/about">
                      About Us
                     </NavItem>
                     <NavItem eventKey={3} componentClass={Link} href="/contact" to="/contact">
                      Contact Us
                     </NavItem>
                     <NavItem eventKey={3} componentClass={Link} href="/contact" to="/contact">
                     <Image src={callicon}  />
                       +123 777 111
                     </NavItem>
                     <NavItem eventKey={3} componentClass={Link} href="/contact" to="/contact">
                     <Link to ="/contact">
                      <Button bsStyle="normal">Get In Touch</Button>
                    </Link>
                     </NavItem>
                  </Nav>
              </Navbar.Collapse>
            </Navbar>
        )
    }
}