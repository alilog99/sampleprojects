import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';


class Cards extends Component {
    constructor(props) {
        super(props);
        this.state={
           
        }
    }
    render() {
        return (
            <div>
                <Card  >
            <CardActionArea>
              <CardMedia 
                component="img"
                alt="app dev"
                className="media"
                image={this.props.cardImage}
                title={this.props.cardTitle}
                style={{ width: 55,marginLeft: 50 , marginTop:10 , minHeight:42}}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2" style={{height:37}}>
                  {this.props.cardName}
                  
                </Typography>
              </CardContent>
            </CardActionArea>
           
          </Card>
            </div>
        );
    }
}

export default Cards;