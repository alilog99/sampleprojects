import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CreateTextElement from '../../../common/typography/element_white';
import Calender from '../booking_table/date_popup';
import CardService from '../../Cards_jp/Cards_Service';
import Motimg from '../../../../../assets/imgs/Motimg.png';
import Mot7img from '../../../../../assets/imgs/Mot7img.png';
import mot_com from '../../../../../assets/imgs/mot_com.png';
import CardPro from '../../Cards_jp/Cards_Pro';



const styles = {
    root: {
      flexGrow: 1,
     
    },
    menuButton: {
      marginLeft: -18,
      marginRight: 10,
    },
    Bar:{
        backgroundColor:'#3786e1',
        boxShadow:' 0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)'
        
    },
    spacer:{
        flexGrow: '1'
    },
    CustomDense:{
      minHeight:'30px'
    },

  };
class ManualBooking extends React.Component{
    constructor(props){
        super(props)
        
    }
  
    render(){
        
          const { classes} = this.props;
       
        return(
            <div  className="Manualbook" style={{marginTop: '10px',border: '1px solid',    borderColor: 'gray', borderRadius: '6px', overflow: 'hidden',minHeight: '438px'}}>
                 <AppBar position="static" 
                 classes={{colorPrimary:classes.Bar}}
                  >
        <Toolbar variant="dense"
          classes={{dense:classes.CustomDense}}
        >
          <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" >
            
          </IconButton>
          
          <CreateTextElement
            element = "h5"
            style={{margin: '0'}}
            fontSize = "16px"
           
            >
              SELECT DATE
             </CreateTextElement>
             <div className={classes.spacer} />
             
             <Calender 
              color = "white"
             />
        </Toolbar>
      </AppBar>

             <div className="Manual_Cards" style ={{ margin:"23px 12.5px 12.5px 25px",    textAlign: "center" }}>
                <CardService
                    serviceImage = {Motimg}
                    serviceTitle = "MOT"
                    availability={true}
                 
                   
                />
                <CardService
                    serviceImage = {Mot7img}
                    serviceTitle = "Class 7 MOT"
                    availability={true}
                   
                />

                 <CardService
                    serviceImage = {mot_com}
                    serviceTitle = "Class 7 MOT"
                    availability={false}
                   
                />
              
                </div>
            </div>
        
        )
    }

}

ManualBooking.defaultProps = {
    classes: PropTypes.object.isRequired,
}

ManualBooking.propTypes = {
    style: PropTypes.object
    
}

export default withStyles(styles)(ManualBooking);