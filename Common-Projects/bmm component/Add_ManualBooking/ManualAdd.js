import React from 'react';
import PropTypes from 'prop-types';
import DrawerContainer from '../../containers/drawer_container/drawer_container';
import   ManualBooking  from '../Add_ManualBooking/Manual_book';

class ManualAdd extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <div style={this.props.style}>
                <DrawerContainer>
                    <ManualBooking/>
                </DrawerContainer>
            </div>
        )
    }

}

ManualAdd.defaultProps = {

}

ManualAdd.propTypes = {
    style: PropTypes.object
}

export default ManualAdd;