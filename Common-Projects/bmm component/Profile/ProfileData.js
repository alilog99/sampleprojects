import React from 'react';
import PropTypes from 'prop-types';
import Proimg from '../Profile/profileimg';
import Prof from '../../../../assets/imgs/logo_dark.png'
import CreateTextElement from '../../common/typography/element_white';
import CardService from '../../etc_jp/Cards_jp/Cards_Service';
import Motimg from '../../../../assets/imgs/MotCheck.png';
import Mot7img from '../../../../assets/imgs/forgot_pass_background.png';
import mot_com from '../../../../assets/imgs/MotMajor.png';

class ProfileInfo extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <div className="container" style={this.props.style}>
                <div className="row" style={{marginTop:'15px'}}>             
                  <div className="col-sm-12 col-md-2 col-lg-2">
                   <Proimg  
                 Proimage={Prof} />
                 </div>
                 <div  className="col-sm-12 col-md-5 col-lg-5" style={{display:'flex', alignItems:'center'}}>              
                
                 <div >
                 <CreateTextElement 
                  element='h5'
                  color='black'
                  fontSize= '15px'
                  style={{marginTop:'14px'}}
                 >
                  <strong>BOSH SERVICES</strong>
                 </CreateTextElement>
                 <div  >
                 <i style={{color:'#3786e1'}} class="fas fa-map-marker-alt"></i>
                 <CreateTextElement 
                  element='p'
                  color='black'
                  fontSize= '15px'
                  style={{marginLeft: '6px',marginBottom: '6px', display: 'inline-block'}}
                 >
                  
                    Belgrave Road,United Kingdom
                 
                 </CreateTextElement>
                 </div>
                 <div>
                 <i style={{color:'#3786e1'}}  class="fas fa-phone"></i>
                 <CreateTextElement 
                  element='p'
                  color='black'
                  fontSize= '15px' 
                  style={{marginLeft: '6px',marginBottom: '6px', display: 'inline-block'}}
                 >
                 
                       +97-234-654-98 
                 
                 </CreateTextElement>
                 </div>
                 <div>
                 <i style={{color:'#3786e1'}} class="fas fa-envelope"></i>
                 <CreateTextElement 
                  element='p'
                  color='black'
                  fontSize= '15px'
                  style={{marginLeft: '6px',marginBottom: '6px', display: 'inline-block'}} 
                 >
                
                      Belgrave Road,United Kingdom 
                 
                 </CreateTextElement>
                </div>
                 </div>

                
                 </div>
                 <div  className="col-sm-12 col-md-2 col-lg-5" style={{textAlign: 'right'}}>              
                <CreateTextElement 
                  element='h5'
                  color='black'
                  fontSize= '15px'
                  style={{marginTop : '20px',marginRight: '12px',display: 'inline-block'}}
                 >
                   <a href="#" style={{textDecoration : 'underline'}}> Edit Profile  </a>    
                  
                 </CreateTextElement>
                 <div style={{display: 'inline-block'}}>
                      < i style={{color:'#3786e1',overflow: 'hidden',border: '1px solid rgb(216, 213, 213)',borderRadius:'50%' ,height:'25px',width:'25px',display: 'flex',alignItems: 'center',justifyContent:'center'}} class="fas fa-pencil-alt"></i>
                      </div>
                 </div>
                 </div>
                 <div className="row">
                  <div className="col-12">
                   <CreateTextElement
                    element='p'
                    color='black'
                    fontSize='15px'
                    style={{marginTop : '45px'}}
                   >
                   There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                   </CreateTextElement>
                  
                  </div>
                 </div>
                 <div className="row">
                   <div className="col-12">
                     <CreateTextElement 
                        element='h5'
                        color='black'
                        fontSize= '15px'
                        style={{marginTop : '35px'}}
                     >
                        <strong>Additional Services</strong>
                     </CreateTextElement>
                   </div>
                    <div className="col-12">
                    <div className="Manual_Cards" style ={{ margin:"23px 12.5px 12.5px -25px" }}>
                    <CardService
                    serviceImage = {Motimg}
                    serviceTitle = "MOT"
                    availability={true}
                 
                   
                />
                <CardService
                    serviceImage = {Mot7img}
                    serviceTitle = "MOT Mminor"
                    availability={true}
                   
                />

                 <CardService
                    serviceImage = {mot_com}
                    serviceTitle = "MOT Major"
                    availability={true}
                   
                />
                </div>

                    </div>
                 </div>
            </div>
        )
    }

}

ProfileInfo.defaultProps = {

}

ProfileInfo.propTypes = {
    style: PropTypes.object
}

export default ProfileInfo;