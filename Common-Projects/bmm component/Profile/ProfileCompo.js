import React from 'react';
import PropTypes from 'prop-types';


const styles = {
   
      
        
       
        
      
        
        avatar: {
          borderRadius: '50%',
          overflow: 'hidden',
          display: 'inline-flex',
          marginBottom: '15px',
          border: '1px solid #000',
         
            width: '100%'
          
        },
      
      
  };



  
  




var user = {
    basicInfo: {
     
     
     
  
      photo: "http://lorempixel.com/500/500/people",
      
    }
  }
  
  
  class Avatar extends React.Component {
  
    render() {
        const { classes} = this.props;
      var image = this.props.image,
          style = {
            width: this.props.width || 50,
            height: this.props.height || 50,
            
          };
           
      
      if (!image) return null;
      
      return (
       <div className="avatar" style={style}>
            
             <img src={this.props.image}  style={{borderRadius: '50%',overflow: 'hidden',display: 'inline-flex',marginBottom: '15px',border: '1px solid #000',width: '100%'}}/> 
        </div>
      );
    }
  }
  
 
class ProfileCompo extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        const { classes} = this.props;
        var info = this.props.info;
        if (!info) return null;
        return(
            <div style={this.props.style}>
                
                <div className="top">
                  <Avatar 
                    image={info.photo} 
                    width={100}
                    height={100}
                    style={this.props.avatar}
                  /> 
                
            
                  
                </div>
          
        
            </div>
        )
    }

}








  
  
  class UserProfile extends React.Component {
    render() {
        const { classes} = this.props;
      return (
        <div id="user-profile">
          <ProfileCompo info={user.basicInfo}
           style={this.props.userprofile}
          />
        </div>
      )
    }
  }
  

  export default UserProfile;
  