import React from 'react';
import PropTypes from 'prop-types';




class Profileimg extends React.Component{
    constructor(props){
        super(props)
    }

    render(){

        
        return(
            <div className="profileImage"  style={{ borderRadius: '50%',overflow: 'hidden',border: '1px solid rgb(216, 213, 213)',   height : '150px',width: '150px',display: 'flex',alignItems: 'center',justifyContent:'center'}}>
             <img src={this.props.Proimage} style={{height: "50px"}} /> 
     
            </div>
        )
    }

}

Profileimg.defaultProps = {

}

Profileimg.propTypes = {
    style: PropTypes.object
}

export default Profileimg;