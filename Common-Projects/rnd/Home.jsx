import React, {Component} from 'react'
import {Link , Route} from 'react-router-dom';
import {Jumbotron, Grid,  Col, Image, Button } from 'react-bootstrap';
import '../pages/Home.css';
import Navbar from '../components/CustomNavbar';
import Arrow from '../assets/arrow.png';
import Typography from '@material-ui/core/Typography';
import Displaycards from './displaycards';
import Aboutimg from '../assets/Aboutusimage2.png';
import Arw from '../assets/arrowblack.png';
import Cmpname from '../assets/companiesname.png';
import Pro from '../assets/projectsimg1.png';
import Pro2 from '../assets/projectsimg2.png';
import Clint from '../assets/client.png';
import Imageslider from '../components/imageslider/imageslider';
import test from '../assets/testimo.png';
import Logo from '../assets/logo.png';
import { Nav, NavItem } from 'react-bootstrap';

export default class Home extends Component{
    
    
    render(){ 
        
        return(
            
            <div className="container">
            <Grid xs={12} sm={4} lg={12} md={8} >
           
            <Col   >
                <Jumbotron >    
                   <Route exact={true} path='/' render={() => (
                      <div className="Home">
                        <Navbar />
                      </div>
                    )}/>

                    <div className="textstart">
                    <h2>Revolution The <br></br>World With Virtual<br></br> &amp; Augmented Reality </h2>
                    <p>We, at RNDTEK, work hard to shape your ideas<br></br>dignity on interactive interfaces for your <br></br>audience internationally</p>
                    <Link to ="/about">
                      <Button bsStyle="primary">Get Started <Image src={Arrow}  /></Button>
                    </Link>
                    </div>
                </Jumbotron>
                </Col>
                
             <Grid container spacing ={20}>
             
             <Grid item xs={2} className="products">
             <Grid item xs={8}>
             <Grid item xs={2}> 
                <Typography variant="subheading" style={{marginLeft: 165,fontSize: 16,color:'white',marginTop: 190}} >
                    Thriving To Achieve Smart,<br/>
                </Typography>
               <Typography component="h2" variant="display3" gutterBottom  style={{color: 'white',fontSize: 24,marginLeft: 164}}>
                    Unique &amp; State Of The Art Solutions<br/>
               </Typography>
               <Typography variant="body1" gutterBottom align="left" style={{color: 'white',fontSize: 12,marginLeft: 166}}>
                   Fusce at hendrerit leo. Suspendisse nisl libero, Sodales a porttitor non, <br/>
                   Viverra ac velit. Fusce mattis elit sed ex mollis. id tincidunt eros fermentum.<br/>
                   Sed ut magna dolor. Proin condimentum massa tortor, in sagittis arcu<br/>
                   maximus non. In sem ante, facilisis et suscipit eu, suscipit eget dui. Sed<br/>
                   maximus, orci quis elementum tincidunt, massa quam vestibulum nibh,<br/>
                   vitae tristiue purus lectus nec augue.
                   </Typography>
                 
                   <Grid item xs={6} className="Cards">
                  <Displaycards  />
                  </Grid>
                  <Link to ="/portfolio">
                      <Button bsStyle="primary"  style={{marginLeft: 162,marginTop: -195}}>Get Started <Image src={Arrow}  /></Button>
                    </Link>
                </Grid>
              
                </Grid>
                </Grid>
                </Grid>
               <Grid item xs={2} className="Aboutus">
             
                   <Image src={Aboutimg} alt="aboutusimg2" style={{float: 'right',width:664,height: 575}} />
                   <div className="Txtbaout">
                        About Us
                     </div>
                     <div className="Abtdiscrip">
                     <p>You have got an idea?</p>
                     <h2>We can turn it into reality </h2>
                     <Typography variant="body1" gutterBottom align="left" style={{color: 'white',fontSize: 12,marginLeft: 140,marginTop:1}}>
                   Fusce at hendrerit leo. Suspendisse nisl libero, Sodales a porttitor non, <br/>
                   Viverra ac velit. Fusce mattis elit sed ex mollis. id tincidunt eros fermentum.<br/>
                   Sed ut magna dolor. Proin condimentum massa tortor, in sagittis arcu<br/>
                   maximus non. In sem ante, facilisis et suscipit eu, suscipit eget dui. Sed<br/>
                   maximus, orci quis elementum tincidunt, massa quam vestibulum nibh,<br/>
                   vitae tristiue purus lectus nec augue.
                   </Typography>
                   <Link to ="/about">
                      <Button bsStyle="primary"  style={{marginLeft: 128,marginTop: 0}}>Get Started <Image src={Arw}  /></Button>
                    </Link>
                </div>
                
               </Grid>
               <Grid className="Brands" >
                     <div className="brndtext" >
                           working with some great brand
                     </div>

                      <Image src={Cmpname} alt="companies name" />
                 </Grid>
                 <Grid className="Prod">
                 <Image src={Pro} alt="Projectimg1" style={{width:1350}}  />
                 <Image src={Pro2} alt="Projectimg2" style={{width:1350}}  />
                 </Grid>
              <Grid item xs={2} className="Testimonial">
                    <p>Our Testimonials</p>
                     <h2>What Our Valuable Clients Say </h2>
                     <Image src={Clint} alt="clint"  style={{ marginLeft: 555,height: 126}}  />
                     <Typography variant="body1" gutterBottom align="center" style={{color: 'white',fontSize: 20,margin: 40,marginTop:1}}>
                     Fusce at hendrerit leo. Suspendisse nisl libero, Sodales a porttitor non, 
                   Viverra ac velit. Fusce mattis elit sed ex mollis. id tincidunt eros fermentum.
                   Sed ut magna dolor. Proin condimentum massa tortor, in sagittis arcu
                   maximus non. In sem ante, facilisis et suscipit eu, suscipit eget dui. Sed
                   maximus, orci quis elementum tincidunt, massa quam vestibulum nibh,
                   vitae tristiue purus lectus nec augue.
                   </Typography>
                  
              </Grid>
               <Grid className="footer" >
               <Link to ="/portfolio" >
                  <h6> Support</h6>
                   </Link>
               <Image src={Logo} style={{width:137, marginLeft:96,marginTop:-26}} />

              <p>  Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. USA &amp; CAN:1-888-123-4567<br/> Address: 34 Brokel Rd.NY </p>
             <div className="footdata">
                   
                     </div>
               </Grid>
            </Grid>
            </div>
        )
    }
}