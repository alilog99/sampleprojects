import React, {Component} from 'react'
import {Link , Route} from 'react-router-dom';
import {Jumbotron, Grid,  Col, Image, Button } from 'react-bootstrap';
import '../pages/Home.css';
import Navbar from '../components/CustomNavbar';
import Arrow from '../assets/arrow.png';
import Typography from '@material-ui/core/Typography';
import Displaycards from './displaycards';
import Aboutimg from '../assets/Aboutusimage2.png';




export default class Home extends Component{
    
    
    render(){ 
        
        return(
            
            <div className="container">
            <Grid xs={12} sm={4} lg={12} md={8} >
           
            <Col   >
                <Jumbotron >    
                   <Route exact={true} path='/' render={() => (
                      <div className="Home">
                        <Navbar />
                      </div>
                    )}/>

                    <div className="textstart">
                    <h2>Revolution The <br></br>World With Virtual<br></br> &amp; Augmented Reality </h2>
                    <p>We, at RNDTEK, work hard to shape your ideas<br></br>dignity on interactive interfaces for your <br></br>audience internationally</p>
                    <Link to ="/about">
                      <Button bsStyle="primary">Get Started <Image src={Arrow}  /></Button>
                    </Link>
                    </div>
                </Jumbotron>
                </Col>
                
             <Grid container spacing ={20}>
             
             <Grid item xs={2} className="products">
             <Grid item xs={8}>
             <Grid item xs={2}> 
                <Typography variant="subheading" style={{marginLeft: 165,fontSize: 16,color:'white',marginTop: 190}} >
                    Thriving To Achieve Smart,<br/>
                </Typography>
               <Typography component="h2" variant="display3" gutterBottom  style={{color: 'white',fontSize: 24,marginLeft: 164}}>
                    Unique &amp; State Of The Art Solutions<br/>
               </Typography>
               <Typography variant="body1" gutterBottom align="left" style={{color: 'white',fontSize: 12,marginLeft: 166}}>
                   Fusce at hendrerit leo. Suspendisse nisl libero, Sodales a porttitor non, <br/>
                   Viverra ac velit. Fusce mattis elit sed ex mollis. id tincidunt eros fermentum.<br/>
                   Sed ut magna dolor. Proin condimentum massa tortor, in sagittis arcu<br/>
                   maximus non. In sem ante, facilisis et suscipit eu, suscipit eget dui. Sed<br/>
                   maximus, orci quis elementum tincidunt, massa quam vestibulum nibh,<br/>
                   vitae tristiue purus lectus nec augue.
                   </Typography>
                 
                   <Grid item xs={6} className="Cards">
                  <Displaycards  />
                  </Grid>
                  <Link to ="/portfolio">
                      <Button bsStyle="primary"  style={{marginLeft: 162,marginTop: -195}}>Get Started <Image src={Arrow}  /></Button>
                    </Link>
                </Grid>
              
                </Grid>
                </Grid>
                </Grid>
               <Grid item xs={2} className="Aboutus">
             
                   <Image src={Aboutimg} alt="aboutusimg2" style={{float: 'right',width:664,height: 575}} />
                   <div className="Txtbaout">
                        About Us
                     </div>
                   
               </Grid>
            
            </Grid>
            </div>
        )
    }
}