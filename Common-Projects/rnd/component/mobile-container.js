import React from 'react'
import PropTypes from 'prop-types'

class MobileContainer extends React.Component{
    constructor(props){
        super(props)

    }
    backgroundStyle = {
        background: 'url('+this.props.backgroundImage+')',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        minHeight: '500px',
        position: 'relative'
    }
    

    render(){
        return(
            <div style={{...this.backgroundStyle,...this.props.style}}>
                         <div className="container">
                    {this.props.children}
                
            </div>
        </div>
        )
    }
}

MobileContainer.defaultProps = {

}

MobileContainer.propTypes = {
    style: PropTypes.object
}

export default MobileContainer;