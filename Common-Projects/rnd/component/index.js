import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Button } from '@storybook/react/demo';
import 'bootstrap/dist/css/bootstrap.css'
import BackgroundContainer from '../components/ui/common/containers/background_container';
import Grid from '@material-ui/core/Grid';
import bannerImg from '../assets/imgs/banner.png'
import CreateTextElement from '../components/ui/common/typography/create_text_element';
import { themeGreenish, themeBlue } from '../components/constants/colors';
import { bannerString } from '../components/constants/strings';
import SimpleCard from '../components/ui/etc/Card'
import  MobileContainer from '../components/ui/common/containers/mobile-container';
import  mobile from '../assets/imgs/projectsimg1.png';
import  mobileimg from '../assets/imgs/projectsimg2.png';
import  Companies from '../assets/imgs/companiesname.png';
require('../assets/css/main.css')



storiesOf('Button', module)
  .add('with text', () => (
    <Button onClick={action('clicked')}>Hello Button</Button>
  ))
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}><span role="img" aria-label="so cool">😀 😎 👍 💯</span></Button>
  ))
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}><span role="img" aria-label="so cool">😀 😎 👍 💯</span></Button>
  ));   




storiesOf('Containers', module)
  .add('Background Container', () => (
   <BackgroundContainer
      backgroundImage = {bannerImg}
      opacity = ".85"
      style={{padding: '150px 0'}}
      > 

      
      <Grid container spacing={16}>
        <Grid item md={6}>

        <CreateTextElement
          element="h5"
          fontSize= "20px"
          >
            Thriving To Achieve Smart, 
        </CreateTextElement>
        
        <CreateTextElement
          element="h4"
          fontSize= "30px"
          >
            Unique & State Of The Art Solutions
        </CreateTextElement>

        <CreateTextElement
          element="p"
          fontSize= "15px"
          style={{lineHeight: '2.1'}}
          >
            {bannerString}
        </CreateTextElement>
        </Grid>
        <Grid item md={6}>
          <SimpleCard/>
        </Grid>
        <Grid item md={6}>
          <SimpleCard/>
        </Grid>
      </Grid>
      

    </BackgroundContainer>
  ))

storiesOf('PageContent', module)
  .add('mobile-container', () => (
    <div>

    <MobileContainer 
     backgroundImage ={Companies}
     style={{padding: '10px 0'}}
     >
      <Grid>
     
      </Grid>
     
      
  </MobileContainer> 



      <MobileContainer 
     backgroundImage ={mobile}
     style={{padding: '150px 0'}}
     >
      <Grid>
     
      </Grid>
     
      
  </MobileContainer> 

<MobileContainer 
backgroundImage ={mobileimg}
style={{padding: '150px 0'}}
>
 <Grid>

 </Grid>

 
</MobileContainer>

    </div>
 
  
  ))

