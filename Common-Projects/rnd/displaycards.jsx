import React from 'react';
import { Grid, Row, Col} from 'react-bootstrap';
import Cards from '../components/cards';
import Appdev from '../assets/Appdevelpmenticon.png';
import Webdev from '../assets/webdevelomenticon.png';
import Digm from '../assets/digitalmarketingicon.png';
import Game from '../assets/gamedevicon.png';
import Vrapp from '../assets/vrappicon.png';
import Dig from '../assets/digmarkicon.png';


const Displaycards = () => {
    return (
        <Grid>
            <div  className="cardsss">                
                <Row className="show-grid text-center" >

               

                 <Col xs={2} className="pwerson-wrapper" style={{float: 'right', marginTop: -30, marginRight: 578, width: 196 , minHeight:40}}>
                 <Cards cardTitle="hello" cardImage={Appdev} cardName="App Development" />   
                                                        
                 </Col>
                 <Col xs={2} className="pwerson-wrapper" style={{float: 'right', marginTop: -30, marginRight: -375, width: 196,  minHeight:40}}>
                 <Cards cardTitle="hello" cardImage={Webdev} cardName="Web Development" style={{minHeight:44}}  />      
                                                        
                 </Col>
                 <Col xs={2} className="pwerson-wrapper" style={{float: 'right', marginTop: -30, marginRight: -553, width: 196, minHeight:40}}>
                     <Cards cardTitle="hello" cardImage={Digm} cardName="Digital Marketing" />    
                                                        
                 </Col>
                 <Col xs={2} className="pwerson-wrapper" style={{float: 'right', marginTop: 118, marginRight: -195, width: 196 , minHeight:40}}>
                     <Cards cardTitle="hello" cardImage={Game} cardName="Game Development" />    
                                                        
                 </Col>
                 <Col xs={2} className="pwerson-wrapper" style={{float: 'right', marginTop: 118, marginRight: -376, width: 196, minHeight:40}}>
                     <Cards cardTitle="hello" cardImage={Vrapp} cardName="VR Application" />    
                                                        
                 </Col>
                 <Col xs={2} className="pwerson-wrapper" style={{float: 'right', marginTop: 118, marginRight: -554, width: 196, minHeight:40}}>
                     <Cards cardTitle="hello" cardImage={Dig} cardName=" Marketing" />    
                                                        
                 </Col>
                
                 
            
                </Row>
                
              
            
        </div>
        </Grid>
    );
};

export default Displaycards;