import React from 'react'
import PropTypes from 'prop-types'
import  OurServ1 from '../portfolio/OurSer1';
import CenteredTabs  from '../portfolio/tabsbar';
import PortGrid from '../portfolio/portfoliogrid'
import Back from '../../../../../assets/imgs/portfolioimg/img1.png';
import Mob from '../../../../../assets/imgs/portfolioimg/img2.png';
import Lap from '../../../../../assets/imgs/portfolioimg/img3.png';
import Lap1 from '../../../../../assets/imgs/portfolioimg/img4.png';
import Smart from '../../../../../assets/imgs/portfolioimg/img5.png';
import Smart1 from '../../../../../assets/imgs/portfolioimg/img6.png';
import FooterCompo from '../our services/footercom';

class Portfolio extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>
            <OurServ1 />

            <CenteredTabs />
            <div style={{textAlign: 'center', paddingTop: '16px'}}>
                <PortGrid title= "ADMIRALTY H-NOTE" name="INVESTMENT" backImg = {Back}/>
                <PortGrid title="Hawkins" name="FORENSIC INVESTIGATION" backImg = {Mob}/>
                <PortGrid title="Hamshire" name="STRAGETY & PLAN" backImg = {Lap}/>
                <PortGrid title="Accu-Mile" name="MARKETING" backImg = {Lap1}/>
                <PortGrid title="O2" name="FININCIAL SERVICE" backImg = {Smart}/>
                <PortGrid title="Discover-Varmints"  name="PROPERTY VALUATION" backImg = {Smart1}/>
            </div>
            <FooterCompo />
            </div>

        )
    }
}

Portfolio.defaultProps = {

}

Portfolio.propTypes = {
    style: PropTypes.object
}

export default Portfolio;