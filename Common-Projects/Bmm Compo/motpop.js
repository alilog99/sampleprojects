import React from 'react';
import PropTypes from 'prop-types';
import ModalLayout from '../../common/modal/modal_layout'
import CreateTextElement from '../../common/typography/element_white';
import { darkBlue, themeGreen } from '../../../constants/colors';
import ButtonTemplate from '../../common/buttons/button_template';
import brandLogo from '../../../../assets/imgs/website_brand.png'
import Avatar from '@material-ui/core/Avatar';
import GrayTextarea from '../../common/inputs/gray_textarea/gray_textarea';

class MOTPOP extends React.Component{
    constructor(props){
        super(props)
        this.state={
            modalHidden: false,
            ratingValue: 0,
            feedback_field: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleRatingChange = this.handleRatingChange.bind(this)
    }
    
    buttonStyle={
        width: '100%',
        height: '34px',
        marginTop: '15px'
    }

    bolderText = {
        color: '#121212'
    }
    bigAvatar= {
        width: 70,
        height: 70,
      }

    handleSubmit(e){
        e.preventDefault();
    }

    handleFieldChange(e){
        this.setState({
            feedback_field: e.target.value
        })
    }

    handleRatingChange(value){
        this.setState({
            ratingValue: value
        })
    }



    render(){
        return(
            <div style={this.props.style}>
                <ModalLayout
                    hidden={this.state.modalDisplay}
                    style={{height: '500px',width: '350px'}}
                    modalTitle="New Order"
                    handleBackgroundClick = {this.props.handleBackgroundClick}
                    >
                    <form onSubmit={this.handleSubmit}>
                    <div>
                <div>
                    <Avatar style={this.bigAvatar} sizes = {[78, 78]} style={{backgroundColor:'blue',textAlign:'center',padding:'35px',marginLeft:'94px'}}>
                  <CreateTextElement
                    element = 'i'
                    fontSize = '44px'
                    className = 'fas fa-car-alt'/>
                </Avatar>

                </div>


              
                <div>
                        <CreateTextElement
                            element="h5"
                            fontSize="15px"
                            style={{textAlign:'center',marginTop:'20px'}}
                            color="Blue"
                            >
                            Congratulation You have 
                        </CreateTextElement>
                        
                        
                        <CreateTextElement
                            element="h5"
                            fontSize="15px"
                            style={{textAlign:'center'}}
                            color="Blue"
                            >
                            a new MOT Service order
                        </CreateTextElement>
                        </div> 
                  
                        <div className="row">
               <div className="col-6">
               <CreateTextElement
                            element="h4"
                            fontSize="15px"
                            style={{marginTop: '20px'}}
                            color="black"
                            >
                            Service
                        </CreateTextElement>
                        
                            <CreateTextElement
                                element="p"
                                fontSize="15px"
                                style={{margin: '0',display:'inline-block'}}
                                color="#777777"
                                >
                                MOT Service
                            </CreateTextElement>
                            
                            <CreateTextElement
                                element="h5"
                                fontSize="15px"
                                style={{marginTop:'20px'}}
                                color="black"
                                >
                                 Slot Date
                            </CreateTextElement>
                        
                        
                            <CreateTextElement
                                element="p"
                                fontSize="15px"
                                style={{margin: '0'}}
                                color="#777777"
                                >
                                12th September
                            </CreateTextElement>
                            
                            <CreateTextElement
                                element="h5"
                                fontSize="15px"
                                style={{marginTop:'20px'}}
                                color="black"
                                >
                                 Registration No.
                            </CreateTextElement>
                        
                        
                            <CreateTextElement
                                element="p"
                                fontSize="15px"
                                style={{margin: '0'}}
                                color="#777777"
                                >
                                MOT Service 
                            </CreateTextElement>
                            
                            <CreateTextElement
                                element="h5"
                                fontSize="15px"
                                style={{marginTop:'30px'}}
                                color="blue"
                                >
                                Price 18 $
                            </CreateTextElement>
               
                        </div>

                        
                            <div className="col-6">
                            <CreateTextElement
                            element="h4"
                            fontSize="15px"
                            style={{marginTop: '25px'}}
                            color="black"
                            >
                            Username
                             </CreateTextElement>
                        
                            <CreateTextElement
                                element="p"
                                fontSize="15px"
                                style={{margin: '0'}}
                                color="#777777"
                                >
                                John Smith
                            </CreateTextElement>
                            
                            <CreateTextElement
                                element="h5"
                                fontSize="15px"
                                style={{marginTop:'20px'}}
                                color="black"
                                >
                                 Slot Time
                            </CreateTextElement>
                        
                        
                            <CreateTextElement
                                element="p"
                                fontSize="15px"
                                style={{margin: '0'}}
                                color="#777777"
                                >
                                04:00 - 05:00 
                            </CreateTextElement>
                            
                            <CreateTextElement
                                element="h5"
                                fontSize="15px"
                                style={{marginTop:'20px'}}
                                color="black"
                                >
                                 Postal Code
                            </CreateTextElement>
                        
                        
                            <CreateTextElement
                                element="p"
                                fontSize="15px"
                                style={{margin: '0'}}
                                color="#777777"
                                >
                                RG6 6AH 
                            </CreateTextElement>
                            
                            <Avatar style={this.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'Green',marginTop:'20px',marginLeft:'100px'}}>
                            <CreateTextElement
                             element = 'i'
                             fontSize = '22px'
                             className="fas fa-envelope-square"/>
                             </Avatar>
                            </div>
                            </div>  
                    </div>
                    </form>
                </ModalLayout>
            </div>
        )
    }

}

MOTPOP.defaultProps = {

}

MOTPOP.propTypes = {
    handleBackgroundClick:PropTypes.func,
    handleConfirmationClick: PropTypes.func,
    hidden: PropTypes.bool
}

export default MOTPOP;