import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Grow } from '@material-ui/core';
import CreateTextElement from '../../common/typography/element_white';
import { lightPurple } from '../../../constants/colors';

const styles = theme => ({
    root: {
      padding: '16px',
    },
    wrapper: {
      maxWidth: 1450,
    },
    paper: {
     
      padding: '20px',
      margin:'20px'
    },
    bigAvatar: {
      width: 60,
      height: 60,
    },
  });
  
  function AutoGridNoWrap(props) {
    const { classes } = props;
    
    return (
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <Paper className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{padding: '20px',}} >
              <Grid item={12} >
                <Avatar className = {classes.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'blue'}}>
                  <CreateTextElement
                    element = 'i'
                    fontSize = '24px'
                    className = 'fas fa-car-alt'/>
                </Avatar>
              </Grid>
              <Grid item xs={12} style={{display:'flex',alignItems:'center',textAlign:'left', flexWrap: 'wrap'}}>
                <CreateTextElement 
                          element="h5" 
                          fontSize="18px"
                          color="#777777"
                          style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                You simply tell us
                </CreateTextElement>
                <CreateTextElement 
                          element="p" 
                          fontSize="15px"
                          color={lightPurple}
                          style={{margin:'0'}}
                >
                               Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. .Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
                </CreateTextElement>
               </Grid>
            </Grid>
        </Paper>
                     <Paper className={classes.paper}>
                        <Grid container wrap="nowrap" spacing={16}style={{padding: '20px',}} >
                            <Grid item={12} >
                            <Avatar className = {classes.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'green'}}>
                            <CreateTextElement
                                 element = 'i'
                                fontSize = '24px'
                                 className = "fas fa-tag"/>
                            </Avatar>
              </Grid>
              <Grid item xs={12} style={{display:'flex',alignItems:'center',textAlign:'left', flexWrap: 'wrap'}}>
                      <CreateTextElement 
                          element="h5" 
                          fontSize="18px"
                          color="#777777"
                          style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                We'll gets the quotes
                      </CreateTextElement>
                      <CreateTextElement 
                          element="p" 
                          fontSize="15px"
                          color={lightPurple}
                          style={{margin:'0'}}
                      >
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. .Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
                      </CreateTextElement>
               </Grid>
               </Grid>
               </Paper>
               <Paper className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{padding: '20px',}} >
              <Grid item={12} >
                <Avatar className = {classes.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'orange'}}>
                  <CreateTextElement
                    element = 'i'
                    fontSize = '28px'
                    className = 'fas fa-car-alt'/>
                </Avatar>
              </Grid>
              <Grid item xs={12} style={{display:'flex',alignItems:'center',textAlign:'left',element:'div', flexWrap: 'wrap'}}>
                      <CreateTextElement 
                          element="h5" 
                          fontSize="18px"
                          color="#777777"
                          style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                You book a top rated mechanic
                      </CreateTextElement>
                      <CreateTextElement 
                          element="p" 
                          fontSize="15px"
                          color={lightPurple}
                          style={{margin:'0'}}
                      >
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
                      </CreateTextElement>
               </Grid>
               </Grid>
               </Paper>
          </div>
        </div>
      
    );
  }
  
  AutoGridNoWrap.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(AutoGridNoWrap);