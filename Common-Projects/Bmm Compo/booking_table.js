import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import { vividBlue, brightRed, strongOrange, themeGreen } from '../../../constants/colors';
import CreateTextElement from '../../common/typography/element_white';
import BookingDetailsModal from '../../etc_jp/pages/booking_schedule';
import BookingStatusChageModal from '../../etc_jp/modals/booking_status_change_modal';
import EditBookingDetailsModal from '../../etc_jp/modals/edit_booking_details_modal'
import DatePopup from '../../etc_jp/content/booking_table/date_popup';
import moment from 'moment'


let counter = 0;
function createData(name, calories, fat, carbs, protein) {
  counter += 1;
  return { id: counter, name, calories, fat, carbs, protein };
}

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);       
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const rows = [
  { id: 'bId', numeric: false, disablePadding: false, label: 'B. ID' },
  { id: 'slotTime', numeric: false, disablePadding: false, label: 'SLOT TIME' },
  { id: 'custName', numeric: false, disablePadding: false, label: 'CUSTOMER NAME' },
  { id: 'makeModel', numeric: false, disablePadding: false, label: 'MAKE & MODEL' },
  { id: 'regNo', numeric: false, disablePadding: false, label: 'REG. NUM' },
  { id: 'product', numeric: false, disablePadding: false, label: 'PRODUCT' },
  { id: 'price', numeric: false, disablePadding: false, label: 'PRICE' },
  { id: 'claim', numeric: false, disablePadding: false, label: 'CLAIM' }
];

const toolbarHeadStyles = theme => ({
    tableHeadCell: {
        padding: '0',
        paddingLeft: '26px',
        border: "1px solid #e8e8e8"
    }
})

class EnhancedTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  
  render() {
    const { onSelectAllClick, order, orderBy, 
      numSelected, rowCount, classes, enableEdit } = this.props;

    return (
      <TableHead>
        <TableRow>
          {/* <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell> */}
          {rows.map(row => {
            return (
              <TableCell
                key={row.id}
                classes = {{root: classes.tableHeadCell}}
                numeric={row.numeric}
                padding={row.disablePadding ? 'none' : 'default'}
                sortDirection={orderBy === row.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === row.id}
                    direction={order}
                    onClick={this.createSortHandler(row.id)}
                  >
                    <CreateTextElement
                        element = "h5"
                        fontSize = "14px"
                        style = {{margin: '0'}}
                        color = '#777777'
                        >
                        {row.label}
                        </CreateTextElement>
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            );
          }, this)}
          <TableCell
                classes = {{root: classes.tableHeadCell}}
                numeric={false}
                padding={'default'}
              >
                <CreateTextElement
                    element = "h5"
                    fontSize = "14px"
                    style = {{margin: '0'}}
                    color = '#777777'
                    >
                    ACTION
                    </CreateTextElement>
          </TableCell>
          {/*enableEdit? <TableCell
                classes = {{root: classes.tableHeadCell}}
                numeric={false}
                padding={'default'}
              >
                <CreateTextElement
                    element = "h5"
                    fontSize = "14px"
                    style = {{margin: '0'}}
                    color = '#777777'
                    >
                    ACTION
                    </CreateTextElement>
          </TableCell>:''*/}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
};



EnhancedTableHead = withStyles(toolbarHeadStyles)(EnhancedTableHead)

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
    backgroundColor: vividBlue,
    minHeight: '50px'
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
});

class EnhancedTableToolbar extends React.Component {
render(){
    const { numSelected, classes, selectedDate } = this.props;
  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subtitle1">
            {numSelected} selected
          </Typography>
        ) : (
          <CreateTextElement
            element = "h5"
            style={{margin: '0'}}
            fontSize = "16px"
            >
            {selectedDate? moment(selectedDate).format('MMMM Do, YYYY') : 'SELECT DATE'}
            </CreateTextElement>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {/*numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete">
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
          <DatePopup
            color = "white"
            handleDateChange = {this.props.handleDateChange}
            />
        )*/}
      </div>
    </Toolbar>
  );}
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  handleDateChange: PropTypes.func,
  selectedDate: PropTypes.any
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  rowHover: {
    borderTop: "1px solid #e8e8e8",
    borderBottom: "1px solid #e8e8e8",
  },
  table: {
    minWidth: 1020,
    borderCollapse: 'collapse',
    border: "1px solid #e8e8e8",
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  row: {
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
    },
    '&:hover':{
      backgroundColor: 'rgba(0, 0, 0, 0.04) !important'
    },
  },
  tableCell: {
    padding: '0',
    paddingLeft: '26px',
    border: "1px solid #e8e8e8"
  },
  tableActionCell: {
    padding: '0',
    paddingLeft: '26px',
    border: "1px solid #e8e8e8",
    textAlign: 'center'
  },
});

class BookingTable extends React.Component {
  state = {
    order: 'desc',
    orderBy: 'bId',
    selected: [],
    data: [
      {bId: 1, slotTime: '10:00 AM', custName: 'B. Asim Snow', makeModel: 'Toyota Corolla', regNo: 'LEK8358', product: 'MOT', price: 58, claim: 58}, 
      {bId: 2, slotTime: '10:00 AM', custName: 'B. Asim Snow', makeModel: 'Toyota Corolla', regNo: 'LEK8558', product: 'MOT', price: 58, claim: 58},
      {bId: 3, slotTime: '10:00 AM', custName: 'B. Asim Snow', makeModel: 'Toyota Corolla', regNo: 'LEK8758', product: 'MOT', price: 58, claim: 58},
      {bId: 4, slotTime: '10:00 AM', custName: 'B. Asim Snow', makeModel: 'Toyota Corolla', regNo: 'LEK8758', product: 'MOT', price: 58, claim: 58},
    ],
    page: 0,
    rowsPerPage: 7,
    selectedDate: null,
    bdModalHidden: true,
    ebdModalHidden: true,
    bsModalHidden: true
  };

  handlebackgroundClick = (modal) => {
    this.setState({
      [modal]: true
    })
  }

  showModal = (modal) => {
    this.setState({
      [modal]: false
    })
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.data.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  formatStatusText = (status) => {
    return <CreateTextElement
        element = 'h5'
        fontSize = "14px"
        color = {this.statusColors[status].color}
        style={{margin: '0',textDecoration: 'underline', cursor: 'pointer'}}
        >{this.statusColors[status].text}</CreateTextElement>
  }

  formatTableText = (text) => {
    return <CreateTextElement
            element = 'p'
            fontSize = "14px"
            color = {'#777777'}
            style={{margin: '0'}}
            >{text}</CreateTextElement>
  }

  handleDateChange = value => {
        this.setState({
            selectedDate: value
        })
    }
  handleRowClick = (entryId) => {
    // console.log("Row Click: ", entryId)
    this.setState({
      bdModalHidden: false
    })
  }

  handleStatusClick = (e, entryId) => {
    if(this.props.enableEdit){
      e.stopPropagation();
      console.log('Status Click: ', entryId)
      this.setState({
        bsModalHidden: false
      })
    }
  }

  handleEditClick = (e, entryId) => {
    e.stopPropagation()
    console.log('Edit Click: ', entryId);
    this.setState({
      ebdModalHidden: false
    })
  }


statusColors = [{text: 'Checked In', color: vividBlue}, {text: 'In Progress', color:strongOrange}, {text: 'Failed', color:brightRed}, {text: 'Passed', color:themeGreen}]

  render() {
    const { classes, enableEdit } = this.props;
    const { data, order, orderBy, selected, rowsPerPage, page, selectedDate } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar 
            numSelected={selected.length}
            selectedDate = {selectedDate} 
            handleDateChange = {this.handleDateChange} 
            />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              enableEdit = {enableEdit}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
            />
            <TableBody>
              {stableSort(data, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .filter(function(item)
                    {if(selectedDate){return selectedDate.format('yyyy-MM-dd') === 
                        moment(item.date).format('yyyy-MM-dd')} return true;}).map(n => {
                  const isSelected = this.isSelected(n.id);
                  return (
                    <TableRow
                      hover
                      classes = {{hover: classes.rowHover}}
                      tabIndex={-1}
                      className = {classes.row}
                      key={n.bId}
                      selected={isSelected}
                      onClick = {event => this.handleRowClick(n.bId)}
                    >
                      {/* <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell> */}
                      {/* <TableCell component="th" scope="row" padding="none">
                        {n.name}
                      </TableCell> */}
                      <TableCell classes = {{root: classes.tableCell}}>{this.formatTableText(n.bId)}</TableCell>
                      <TableCell classes = {{root: classes.tableCell}}>{this.formatTableText(n.slotTime)}</TableCell>
                      <TableCell classes = {{root: classes.tableCell}}>{this.formatTableText(n.custName)}</TableCell>
                      <TableCell classes = {{root: classes.tableCell}}>{this.formatTableText(n.makeModel)}</TableCell>
                      <TableCell classes = {{root: classes.tableCell}}>{this.formatTableText(n.regNo)}</TableCell>
                      <TableCell classes = {{root: classes.tableCell}}>{this.formatTableText(n.product)}</TableCell>
                      <TableCell classes = {{root: classes.tableCell}}>{this.formatTableText( '£'+ n.price )}</TableCell>
                      <TableCell onClick = {event => this.handleStatusClick(event, n.bId)} classes = {{root: classes.tableCell}}>
                        {this.formatStatusText(n.status)}
                      </TableCell>
                      
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={9} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[7, 15, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
        <BookingDetailsModal 
          modalHidden = {this.state.bdModalHidden} 
          handleBackgroundClick = {event => this.handlebackgroundClick('bdModalHidden')}
          />
        <EditBookingDetailsModal 
          modalHidden = {this.state.ebdModalHidden} 
          handleBackgroundClick = {event => this.handlebackgroundClick('ebdModalHidden')}
          />
        <BookingStatusChageModal 
          modalHidden = {this.state.bsModalHidden} 
          handleBackgroundClick = {event => this.handlebackgroundClick(event)}
          />
      </Paper>
    );
  }
}

BookingTable.defaultProps = {
  enableEdit: true
}

BookingTable.propTypes = {
  classes: PropTypes.object.isRequired,
  enableEdit: PropTypes.bool
};

export default withStyles(styles)(BookingTable);