import React from 'react';
import PropTypes from 'prop-types';
import GraySmBackgroundContainer from '../../etc_jp/containers/gray_container/gray_sm_background_container'
import backgroundImage from '../../../../assets/imgs/signup_background.png'
import CreateTextElement from '../../common/typography/element_white';
import { darkBlue, vividBlue } from '../../../constants/colors';
import UploadImage from '../../common/inputs/upload_images/upload_images';
import InputUnderline from '../../common/inputs/input_underline/input_underline';
import SelectUnderline from '../../common/inputs/select_underline/select_underline';
import ButtonTemplate from '../../common/buttons/button_template';
import Zoom from '@material-ui/core/Zoom';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import InputIcon from '../../common/inputs/input_with_icon/input_icon';
import { withStyles } from '@material-ui/core/styles';


const styles = {
    custom_label: {
        marginBottom: '0'
    },
    custom_label_typo: {
        fontFamily: 'poppins',
        marginLeft: '8px'
    },
    blue_checked: { 
        color: vividBlue,
        '&$checked': {
          color: vividBlue,
        }
    },
    blue_checked: {
        color: vividBlue,
        padding: '2px 12px',
        '&$checked': {
          color: vividBlue,
        },
      },
    checked: {},
  };


class SignUp extends React.Component{
    constructor(props){
        super(props)
        this.state={
            buttonContainer:{display: 'inline-block',marginLeft: '8px',marginBottom:'-25px'},
            zoomIn1: true,
            zoomIn2: false
        }
    }

    style = {
        // marginBottom: '15px'
    }

    styleSelect={
        marginTop: '16px'
    }

    priceFieldStyle = {
        marginBottom: '18px',
        paddingTop: '6px'
    }

    handleNextClick(e){
        this.setState({
            zoomIn1: false,
            zoomIn2: true
        })
    }

    handleBackClick(e){
        this.setState({
            zoomIn1: true,
            zoomIn2: false
        })
    }

    render(){
        console.log(this.state)
        const { classes } = this.props;
        return(
            <React.Fragment>
                <div className = "container">
                <Zoom in={this.state.zoomIn1}>
                        <div style={{display: this.state.zoomIn1?'flex':'none'}} className="row">
                            <div className="col-12 col-sm-12 col-md-12 col-lg-4">
                                <CreateTextElement
                                    element="h3"
                                    color = {darkBlue}
                                    fontSize = '25px'
                                    >
                                    PERSONAL INFO
                                </CreateTextElement>
                                <UploadImage
                                    titleText = "Upload Logo"
                                    description = "100 x 60 pixels"
                                    style={{margin: '38px 0 28px 0'}}
                                    />
                                <UploadImage
                                    titleText = "Upload Banner"
                                    description = "Dimensions Here"
                                    style={{marginTop: '48px',marginBottom: '25px'}}
                                    />
                                <InputUnderline 
                                    label="First Name"
                                    theme="theme-dark"
                                    name="cfName"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Last Name"
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-4">
                                {/* <div style={{margin:'30px'}} className="d-block d-md-none"></div> */}
                                <InputUnderline 
                                    label="Phone Number"
                                    theme="theme-dark"
                                    name="cPhoneNo"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Personal Email"
                                    theme="theme-dark"
                                    name="cEmail"
                                    style={this.style}
                                    />
                                <CreateTextElement
                                    element="h3"
                                    color = {darkBlue}
                                    fontSize = '25px'
                                    style={{marginTop: '47px'}}
                                    >
                                    BUSINESS INFO
                                </CreateTextElement>
                                <InputUnderline 
                                    label="Garage Name"
                                    theme="theme-dark"
                                    name="garageName"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Garage Address"
                                    theme="theme-dark"
                                    name="garageAdd"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Phone Number (Work)"
                                    theme="theme-dark"
                                    name="phNoGarage"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Email Address (Work)"
                                    theme="theme-dark"
                                    name="emailGarage"
                                    style={this.style}
                                    />
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-4">
                                <SelectUnderline 
                                    listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                    label = "Business Days"
                                    theme="theme-dark"
                                    name="bsnisDays"
                                    style={this.styleSelect}
                                    />
                                <SelectUnderline 
                                    listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                    label = "Business Hours From"
                                    theme="theme-dark"
                                    name="bsnisHoursForm"
                                    style={this.styleSelect}
                                    />
                                <SelectUnderline 
                                    listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                    label = "Business Hours To"
                                    theme="theme-dark"
                                    name="bsnisHoursTo"
                                    style={this.styleSelect}
                                    />
                                <SelectUnderline 
                                    listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                    label = "Bank Holidays Hours"
                                    theme="theme-dark"
                                    name="bankHolidays"
                                    style={this.styleSelect}
                                    />
                                <InputUnderline 
                                    label="Company Reg Number"
                                    theme="theme-dark"
                                    name="cRegNo"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="VAT Number"
                                    theme="theme-dark"
                                    name="vatNo"
                                    style={this.style}
                                    />
                                <div className="text-right">
                                    <ButtonTemplate
                                            template="blue"
                                            style={{marginTop: '30px'}}
                                            onClick = {this.handleNextClick.bind(this)}
                                            >
                                            NEXT
                                    </ButtonTemplate>
                                </div>
                                <div style={{marginBottom:'50px'}} className="d-block d-lg-none"></div>
                            </div>
                        </div>
                    </Zoom>
                    <Zoom in={this.state.zoomIn2}>
                        <div
                            style={{display: this.state.zoomIn2?'block':'none'}}
                            >
                            <IconButton style={{    marginLeft: '-20px',marginTop: '-12px', marginBottom: '12px'}} onClick={this.handleBackClick.bind(this)} aria-label="Delete">
                                <i className="fas fa-arrow-left"></i>
                            </IconButton>
                            <div className="row">
                                <div className="col-12 col-sm-12 col-md-12 col-lg-4">
                                    <CreateTextElement
                                        element="h3"
                                        color = {darkBlue}
                                        fontSize = '23px'
                                        style={{marginBottom: '36px'}}
                                        >
                                        PRODUCT / SERVICES
                                    </CreateTextElement>
                                    
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motC4p"
                                        style={this.priceFieldStyle}
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT Class 7" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motC7p"
                                        style={this.priceFieldStyle}
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT Minor Service" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motMinor"
                                        style={this.priceFieldStyle}
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT Interim Service" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motInterim"
                                        style={this.priceFieldStyle}
                                        />
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-4">
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT Major Service" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motMajor"
                                        style={this.priceFieldStyle}
                                        />
                                    <CreateTextElement
                                        element="h3"
                                        color = {darkBlue}
                                        fontSize = '23px'
                                        style={{marginBottom: ''}}
                                        >
                                        GARAGE INFO
                                    </CreateTextElement>
                                    <InputUnderline 
                                        label="Vosa Test Station Number"
                                        theme="theme-dark"
                                        name="vatNo"
                                        style={this.style}
                                        />
                                    <SelectUnderline 
                                        listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                        label = "No. of MOT Ramps"
                                        theme="theme-dark"
                                        name="bankHolidays"
                                        style={this.styleSelect}
                                        />
                                    <SelectUnderline 
                                        listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                        label = "MOT Slot Time"
                                        theme="theme-dark"
                                        name="bankHolidays"
                                        style={this.styleSelect}
                                        />
                                    <InputUnderline 
                                        label="Upload Service Schedule"
                                        theme="theme-dark"
                                        name="vatNo"
                                        style={this.style}
                                        />
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-4">
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="Customer Waiting Area" 
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="Wifi" 
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="Car Services" 
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="Other Repair Work" 
                                        />
                                    <ButtonTemplate
                                        template="blue"
                                        style={{margin: '22px 0',}}
                                        >
                                        SIGNUP
                                    </ButtonTemplate>
                                </div>
                            </div>
                        </div>
                    </Zoom>
                </div>
                {/* <div className ="container" >
                        <div  className="row">
                            <div className="col-4 col-sm-5 col-md-5 col-lg-4">
                                <CreateTextElement
                                    element="h3"
                                    color = {darkBlue}
                                    fontSize = '25px'
                                    style={{marginTop: '28px'}}
                                    >
                                    PERSONAL INFO
                                </CreateTextElement>
                                <UploadImage
                                    titleText = "Upload Logo"
                                    description = "100 x 60 pixels"
                                    style={{margin: '38px 0 28px 0'}}
                                    />
                                <UploadImage
                                    titleText = "Upload Banner"
                                    description = "Dimensions Here"
                                    style={{marginTop: '48px',marginBottom: '25px'}}
                                    />
                                <InputUnderline 
                                    label="First Name"
                                    theme="theme-dark"
                                    name="cfName"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Last Name"
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Phone Number"
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Email address"
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                                <CreateTextElement
                                        element="h3"
                                        color = {darkBlue}
                                        fontSize = '23px'
                                        style={{marginTop: '40px'}}
                                        >
                                        GARAGE INFO
                                    </CreateTextElement>
                                    <InputUnderline 
                                        label="Vosa Test Station Number"
                                        theme="theme-dark"
                                        name="vatNo"
                                        style={this.style}
                                        />
                                    <SelectUnderline 
                                        listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                        label = "No. of MOT Ramps"
                                        theme="theme-dark"
                                        name="bankHolidays"
                                        style={this.styleSelect}
                                        />
                                    <SelectUnderline 
                                        listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                        label = "MOT Slot Time"
                                        theme="theme-dark"
                                        name="bankHolidays"
                                        style={this.styleSelect}
                                        />
                                    <InputUnderline 
                                        label="Upload Service Schedule"
                                        theme="theme-dark"
                                        name="vatNo"
                                        style={this.style}
                                        />
                                </div>
                                <div className="col-4 col-sm-5 col-md-5 col-lg-4">
                                <CreateTextElement
                                    element="h3"
                                    color = {darkBlue}
                                    fontSize = '25px'
                                    style={{marginTop: '28px'}}
                                    >
                                    BUSINESS INFO
                                </CreateTextElement>
                                <InputUnderline 
                                    label="Garage Name"
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Garage address"
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Phone Number (Work)"
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="Email address (Work)"
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                                <SelectUnderline 
                                    listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                    label = "Business Days"
                                    theme="theme-dark"
                                    name="bsnisDays"
                                    style={this.styleSelect}
                                    />
                                <SelectUnderline 
                                    listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                    label = "Business Hours From"
                                    theme="theme-dark"
                                    name="bsnisDays"
                                    style={this.styleSelect}
                                    />
                                <SelectUnderline 
                                    listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                    label = "Business Holidays Hours"
                                    theme="theme-dark"
                                    name="bsnisDays"
                                    style={this.styleSelect}
                                />
                                <SelectUnderline 
                                listOptions = {["Option 1", "Option 2", "Option 3"]} 
                                label = "Bank Holidays Hours To"
                                theme="theme-dark"
                                name="bsnisDays"
                                style={this.styleSelect}
                                />
                                <InputUnderline 
                                    label="Company Reg Number "
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                                <InputUnderline 
                                    label="VAT Number "
                                    theme="theme-dark"
                                    name="clName"
                                    style={this.style}
                                    />
                            </div>
                            <div className="col-4 col-sm-5 col-md-5 col-lg-4">
                            <CreateTextElement
                                    element="h3"
                                    color = {darkBlue}
                                    fontSize = '25px'
                                    style={{marginTop: '28px'}}
                                    >
                                    PRODUCT / SERVICES
                                </CreateTextElement>
                                <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motC4p"
                                        style={this.priceFieldStyle}
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT Class 7" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motC7p"
                                        style={this.priceFieldStyle}
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT Minor Service" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motMinor"
                                        style={this.priceFieldStyle}
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT Interim Service" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motInterim"
                                        style={this.priceFieldStyle}
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="MOT Major Service" 
                                        />
                                    <InputIcon
                                        placeholder="Price"
                                        name="motMajor"
                                        style={this.priceFieldStyle}
                                        />
                                    
                                </div>
                                <div style={{marginTop:'30px' }} className="col-4 col-sm-4 col-md-4 col-lg-12">
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label=" Waiting Area" 
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="Wifi" 
                                        />
                                    <FormControlLabel 
                                        classes={{root: classes.custom_label, label: classes.custom_label_typo}}
                                        control = {
                                            <Checkbox 
                                                value="checkedC" 
                                                classes={{root: classes.blue_checked, checked: classes.checked}}
                                                />
                                        } 
                                        label="Repair / Service" 
                                        />
                                         <div  className="col-4 col-sm-4 col-md-4 col-lg-12">
                                    <ButtonTemplate
                                        template="blue"
                                        style={{margin: '14px 0',}}
                                        >
                                        SIGNUP
                                    </ButtonTemplate>
                                     </div>
                                    </div>
                            </div>
                        </div> */}
            </React.Fragment>
        )
    }

}

SignUp.defaultProps = {

}

SignUp.propTypes = {
    style: PropTypes.object
}

export default withStyles(styles)(SignUp);