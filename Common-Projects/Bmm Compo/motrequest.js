import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Grow } from '@material-ui/core';
import CreateTextElement from '../../common/typography/element_white';
import { lightPurple } from '../../../constants/colors';

const styles = theme => ({
    root: {
      padding: '16px',
    },
    wrapper: {
      maxWidth: 1450,
    },
    paper: {
     
      padding: '20px',
      margin:'20px'
    },
    bigAvatar: {
      width: 60,
      height: 60,
    },
  });
  
  function AutoGridNoWrap(props) {
    const { classes } = props;
    
    return (
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <Paper className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{padding: '20px',}} >
              <Grid item >
                <Avatar className = {classes.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'blue'}}>
                  <CreateTextElement
                    element = 'i'
                    fontSize = '24px'
                    className = 'fas fa-car-alt'/>
                </Avatar>
              </Grid>
              <Grid item xs={4} style={{display:'flex',alignItems:'center',textAlign:'left',element:'div', flexWrap: 'wrap'}}>
                      <CreateTextElement 
                          element="h5" 
                          fontSize="18px"
                          color="#777777"
                          style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                MOT Request
                      </CreateTextElement>
                      <CreateTextElement 
                          element="p" 
                          fontSize="15px"
                          color={lightPurple}
                          style={{margin:'0'}}
                      >
                               You have new MOT Request 
                      </CreateTextElement>
               </Grid>
              <Grid item xs={8} style={{display:'flex',alignItems:'center',flexDirection:'row-reverse'}} >
                        <CreateTextElement 
                             element="p" 
                             fontSize="14px"
                             color="#777777"
                             style={{margin:'0'}}
                         >
                                 3:24 AM
                         </CreateTextElement>
              </Grid>
            </Grid>
          </Paper>
          <Paper className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{padding: '20px',}}>
              <Grid item>
                <Avatar className = {classes.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'green'}}>
                          <CreateTextElement
                             element = 'i'
                             fontSize = '24px'
                             className="fas fa-envelope-square"/>
                </Avatar>
              </Grid>
              <Grid item xs={4}  style={{display:'flex',alignItems:'center',textAlign:'left', flexWrap: 'wrap'}} >
                          <CreateTextElement 
                             element="h5" 
                             fontSize="18px"
                             color="#777777"
                             style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                  New Message
                          </CreateTextElement>
                          <CreateTextElement 
                              element="p" 
                              fontSize="15px"
                              color={lightPurple}
                              style={{margin:'0'}}
                            >
                                  You have new Message Request 
                           </CreateTextElement>
              </Grid>
              <Grid item xs={8} style={{display:'flex',alignItems:'center',flexDirection:'row-reverse'}} >
                           <CreateTextElement 
                                element="p" 
                                fontSize="14px"
                                color="#777777"
                                style={{margin:'0'}}
                            >
                                    3:24 AM
                           </CreateTextElement>
              </Grid>
            </Grid>
          </Paper>
          <Paper className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{padding: '20px',}}>
              <Grid item>
              <Avatar className = {classes.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'orange'}}>
                          <CreateTextElement
                               element = 'i'
                               fontSize = '24px'
                               className = 'fas fa-car-alt'/>
              </Avatar>
              </Grid>
              <Grid item xs={4} style={{display:'flex',alignItems:'center',textAlign:'left',flexWrap: 'wrap'}}>
                          <CreateTextElement 
                                element="h5" 
                                fontSize="18px"
                                color="#777777"
                                style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                     MOT Request
                           </CreateTextElement>
                           <CreateTextElement 
                                 element="p" 
                                 fontSize="15px"
                                 color={lightPurple}
                                 style={{margin:'0'}}
                           >
                                 You have new MOT Request 
                            </CreateTextElement>
              </Grid>
              <Grid item xs={8} xs={8} style={{display:'flex',alignItems:'center',flexDirection:'row-reverse'}} >
                           <CreateTextElement 
                                  element="p" 
                                  fontSize="14px"
                                  color="#777777"
                                  style={{margin:'0'}}
                           >
                                   3:24 AM
                             </CreateTextElement>
              </Grid>
            </Grid>
          </Paper>
          <Paper className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{padding: '20px',}}>
              <Grid item>
              <Avatar className = {classes.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'Blue'}}>
                               <CreateTextElement
                                   element = 'i'
                                   fontSize = '24px'
                                  className = 'fas fa-car-alt'/>
              </Avatar>
              </Grid>
              <Grid item xs={4} style={{display:'flex',alignItems:'center',textAlign:'left',flexWrap: 'wrap'}}>
                               <CreateTextElement 
                                     element="h5" 
                                     fontSize="18px"
                                     color="#777777"
                                     style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                           MOT Request
                               </CreateTextElement>
                               <CreateTextElement 
                                      element="p" 
                                      fontSize="15px"
                                      color={lightPurple}
                                      style={{margin:'0'}}
                                      >
                                      You have new MOT Request 
                                 </CreateTextElement>
              </Grid>
              <Grid item xs={8} xs={8} style={{display:'flex',alignItems:'center',flexDirection:'row-reverse'}} >
                                <CreateTextElement 
                                      element="p" 
                                      fontSize="14px"
                                      color="#777777"
                                      style={{margin:'0'}}
                                >
                                        3:24 AM
                                </CreateTextElement>
              </Grid>
            </Grid>
          </Paper>
          <Paper className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{padding: '20px',}}>
              <Grid item>
              <Avatar className = {classes.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'green'}}>
                                 <CreateTextElement
                                    element = 'i'
                                    fontSize = '24px'
                                    className = "fas fa-envelope-square"/>
              </Avatar>
              </Grid>
              <Grid item xs={4} style={{display:'flex',alignItems:'center',textAlign:'left',flexWrap: 'wrap'}}>
                                    <CreateTextElement 
                                          element="h5" 
                                          fontSize="18px"
                                          color="#777777"
                                          style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                          New Message
                                    </CreateTextElement>
                                    <CreateTextElement 
                                        element="p" 
                                        fontSize="15px"
                                        color={lightPurple}
                                        style={{margin:'0'}}
                                        >
                                        You have new Message Request 
                                    </CreateTextElement>
              </Grid>
              <Grid item xs={8} xs={8} style={{display:'flex',alignItems:'center',flexDirection:'row-reverse'}} >
                                    <CreateTextElement 
                                            element="p" 
                                            fontSize="14px"
                                            color="#777777"
                                            style={{margin:'0'}}
                                          >
                                              3:24 AM
                                    </CreateTextElement>
              </Grid>
            </Grid>
          </Paper>
          <Paper className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{padding: '20px',}}>
              <Grid item>
              <Avatar className = {classes.bigAvatar} sizes = {[55, 55]} style={{backgroundColor:'blue'}}>
                                    <CreateTextElement
                                      element = 'i'
                                      fontSize = '24px'
                                      className = 'fas fa-car-alt'/>
                </Avatar>
              </Grid>
              <Grid item xs={4} style={{display:'flex',alignItems:'center',textAlign:'left',element:'div', flexWrap: 'wrap'}}>
                                    <CreateTextElement 
                                        element="h5" 
                                        fontSize="18px"
                                        color="#777777"
                                        style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                        MOT Request
                                    </CreateTextElement>
                                    <CreateTextElement 
                                        element="p" 
                                        fontSize="15px"
                                        color={lightPurple}
                                        style={{margin:'0'}}
                                        >
                                        You have new MOT Request 
                                    </CreateTextElement>
                  </Grid>
              <Grid item xs={8} style={{display:'flex',alignItems:'center',flexDirection:'row-reverse'}} >
                                      <CreateTextElement 
                                            element="p" 
                                            fontSize="14px"
                                            color="#777777"
                                            style={{margin:'0'}}
                                            >
                                            3:24 AM
                                      </CreateTextElement>
              </Grid>
            </Grid>
          </Paper>
          
        </div>
      </div>
    );
  }
  
  AutoGridNoWrap.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(AutoGridNoWrap);