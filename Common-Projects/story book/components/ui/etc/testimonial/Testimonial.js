import React from 'react'
import PropTypes from 'prop-types'
import Testimonialbanner from '../../../../assets/imgs/test-banner.png';
import { Grid } from '@material-ui/core';
import CreateTextElement from '../../common/typography/create_text_element';
import Testimonialtext from '../../../../assets/imgs/test-text.png';
import { testimonialstring } from '../../../constants/strings';
import TestImage from '../testimonial/test_image';
import Testimonialss from '../testimonial/testimonialCompo';

class Testimonial extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>
 <Testimonialss
      backgroundImage = {Testimonialbanner}
      opacity = ".92"
      style={{padding: '150px 0'}}
      >

      
      <Grid container spacing={16}>
        <Grid item md={1}></Grid>
        <Grid item md={10}>

        <CreateTextElement
          element="h5"
          fontSize= "16px"
          style={{textAlign:'center'}}
          >
            Our Testimonial 
        </CreateTextElement>
        
        <CreateTextElement
          element="h4"
          fontSize= "22px"
          style={{textAlign:'center'}}
          >
          What Our Valueable Clients Say <br/> <br/>
          <img src={Testimonialtext}></img>
        </CreateTextElement>
        

        <CreateTextElement
          element="p"
          fontSize= "12px"
          style={{lineHeight: '2.0', textAlign: 'center'}}
          >
            {testimonialstring}
            <TestImage/> <TestImage/>
        </CreateTextElement>
        
        </Grid>
        <Grid item md={1}></Grid>
      </Grid>
      

    </Testimonialss>
            </div>
        )
    }
}

Testimonial.defaultProps = {

}

Testimonial.propTypes = {
    style: PropTypes.object
}

export default Testimonial;