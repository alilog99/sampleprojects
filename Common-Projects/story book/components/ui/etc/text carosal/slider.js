import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import classNames from 'classnames';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const tutorialSteps = [
  {
    label: <div className="text1">
           <h2 style={{fontSize:'28px'}}>Revolution The <br></br>World With Virtual<br></br> &amp; Augmented Reality </h2>
           <p>We, at RNDTEK, work hard to shape your ideas<br></br>dignity on interactive interfaces for your <br></br>audience internationally</p>
           </div>
  },
  {
    label:  <div className="text2">
           <h2 style={{fontSize:'28px'}}>Revolution The <br></br>World With Virtual<br></br> &amp; Augmented Reality </h2>
           <p>We, at RNDTEK, work hard to shape your ideas<br></br>dignity on interactive interfaces for your <br></br>audience internationally</p>
           </div>
  },
  {
    label:  <div className="text3">
            <h2 style={{fontSize:'28px'}}>Revolution The <br></br>World With Virtual<br></br> &amp; Augmented Reality </h2>
           <p>We, at RNDTEK, work hard to shape your ideas<br></br>dignity on interactive interfaces for your <br></br>audience internationally</p>
           </div>
  },
  {
    label:  <div className="text4">
           <h2 style={{fontSize:'28px'}}>Revolution The <br></br>World With Virtual<br></br> &amp; Augmented Reality </h2>
           <p>We, at RNDTEK, work hard to shape your ideas<br></br>dignity on interactive interfaces for your <br></br>audience internationally</p>
          </div>
  },
  {
    label:  <div className="text5">
           <h2 style={{fontSize:'28px'}}>Revolution The <br></br>World With Virtual<br></br> &amp; Augmented Reality </h2>
           <p>We, at RNDTEK, work hard to shape your ideas<br></br>dignity on interactive interfaces for your <br></br>audience internationally</p>
          </div>
  },
];

const styles = theme => ({
  root: {
    maxWidth: 400,
    flexGrow: 1,
    
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    height: 50,
    paddingLeft: theme.spacing.unit * 4,
   
  },
  customDots:{
    
    flexDirection: 'column',
    marginTop: '-138px',
    marginRight: '389px',
     
  },
  custom_dot:{
    backgroundColor: 'darkgray',
    margin: '2px 2px',
    borderRadius: '50%',
    width: '8px',
    height: '8px',
  },
  dotActive :{
      backgroundColor: '#0071bc',
  }
});

class SwipeableTextMobileStepper extends React.Component {
  state = {
    activeStep: 0,
  };

  handleNext = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep - 1,
    }));
  };

  handleStepChange = activeStep => {
    this.setState({ activeStep });
  };

  render() {
    const { classes} = this.props;
    const {   theme } = this.props;
   
    const { activeStep } = this.state;
    const maxSteps = tutorialSteps.length;

    return (
      <div className={classes.root} style={{    marginTop: '125px',marginLeft: '96px'}}>
        <Paper  className={classes.header} style ={{backgroundColor:'transparent'}}>
          <Typography style={{color:'white',marginTop: '90px'}}>{tutorialSteps[activeStep].label}</Typography>
        </Paper>
        <AutoPlaySwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={activeStep}
          onChangeIndex={this.handleStepChange}
          enableMouseEvents
        >
          {tutorialSteps.map((step, index) => (
            <div key={step.label}>
              
            </div>
          ))}
        </AutoPlaySwipeableViews>
        <MobileStepper
          variant="dots"
          steps={maxSteps}
          position="static"
          activeStep={this.state.activeStep}
          classes={{dot: classes.custom_dot, dotActive: classes.dotActive ,dots:classes.customDots}}
          style ={{backgroundColor:'transparent' ,flexDirection: 'column' ,    marginTop: '125px'}}
         
        />
      </div>
    );
  }
}

SwipeableTextMobileStepper.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(SwipeableTextMobileStepper);