import React from 'react'
import PropTypes from 'prop-types'
import MobileContainer from '../Home page componentts/mobileimgcompo';
import Grid from '@material-ui/core/Grid';
import  mobile from '../../../../../assets/imgs/projectsimg1.png';
import  mobileimg from '../../../../../assets/imgs/projectsimg2.png';
import  Companies from '../../../../../assets/imgs/polizia.png';
import  Myriad from '../../../../../assets/imgs/myriad.png';
import  Film from '../../../../../assets/imgs/fujifilm.png';
import  Sain from '../../../../../assets/imgs/sainsbury.png';
import  Cain from '../../../../../assets/imgs/McCain.png';
import  Honey from '../../../../../assets/imgs/honeywell.png';

class MobileImg extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div >
            <MobileContainer
    
    style={{padding: '10px 0',minHeight: '10px'}}
    >
     <Grid>
         <h6 style={{textAlign:'center',color: 'gray',marginBottom: '-20px'}}>Working with some great Brand</h6>
          <div style={{marginLeft:'80px',verticalAlign:'middle'}}/>
             <img style={{width: '80px',margin:'15px',marginLeft:'40px'}} src={Companies} alt="companies"/>
                 <img  style={{width: '150px',margin:'15px',marginLeft:'20px'}} src={Myriad} alt="myriad"/>
                    <img  style={{width: '150px',margin:'15px',marginLeft:'20px'}}src={Film} alt="film"/>
                 <img  style={{width: '150px',margin:'15px',marginLeft:'20px'}}src={Sain} alt="sainsbury"/>
               <img  style={{width: '150px',margin:'15px',marginLeft:'20px'}}src={Cain} alt="mccain"/>
             <img  style={{width: '200px',margin:'15px',marginLeft:'20px'}}src={Honey} alt="mccain"/>
     </Grid>    
 </MobileContainer> 



     <MobileContainer 
       backgroundImage ={mobile}
        style={{padding: '150px 0',margintop: '80px'}}
     >
       <Grid>
    
       </Grid>
    </MobileContainer> 

<MobileContainer 
backgroundImage ={mobileimg}
style={{padding: '150px 0'}}
>
<Grid>

</Grid>


</MobileContainer>

            </div>
        )
    }
}

MobileImg.defaultProps = {

}

MobileImg.propTypes = {
    style: PropTypes.object
}

export default MobileImg;