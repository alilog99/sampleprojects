import React from 'react'
import PropTypes from 'prop-types'

class Mobileimg extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>

            </div>
        )
    }
}

Mobileimg.defaultProps = {

}

Mobileimg.propTypes = {
    style: PropTypes.object
}

export default Mobileimg;