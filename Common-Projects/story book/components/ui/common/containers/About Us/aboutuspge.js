import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid';
import Btn from '../../button/button';
import ar from '../../../../../assets/imgs/arrow.png';
import about from '../../../../../assets/imgs/aboutusback.png';
import AboutBackTop from '../About Us/aboutus';
import Nav from '../../Navbar/navbar';
import aboutmdl1 from '../../../../../assets/imgs/about.png';
import SecondCompo from '../About Us/AboutComponents/SecondCompo';
import CreateTextElement from '../../typography/create_text_element';
import {AboutString} from '../../../../../components/constants/strings';
import Footer from '../Home page componentts/footer';
import TextboxFooter from '../../../etc/footer text/textbox';
import logo from '../../../../../assets/imgs/rnd_logo.png';
import { footerString } from '../../../../constants/strings';
import Testimonials from '../../../etc/testimonial/Testimonial';
import ThirdComponent from '../About Us/AboutComponents/thirdComponent';
import AboutBrand from '../About Us/AboutComponents/aboutbrand';


class AboutusPage extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div  >
               <AboutBackTop  backgroundImage ={about}
    
                  style={{paddingBottom: '150px'}}
                 >
                    <Grid container spacing={16}>
                      <Grid item  md ={12} lg={12} xs={12} >
                        <Nav  />
                         <p style ={{color:'white' , fontStyle:'Italic', fontSize:'42px', textAlign: '-webkit-center'}}> We Are <big><strong>RND HUB</strong></big> </p>
                         <p style={{color:'white', fontSize:'20px', textAlign:'center'}}>Fusce at hendrerit leo. Suspendisse nisl libero, sodales a porttitor non, viverra ac velit. Fusce mattis elit sed ex mollis, id tincidunt eros fermentum.</p>
                      </Grid>
                     <div  style={{textAlign: 'center',display: 'block',width: '100%'}}>   
                        <Btn  Gname="Get Started" imgarr={ar} style={{minHeight: '100px',maxHeight: '123px',color:'white', backgroundColor:'#0071bc' , minHeight: '59px', width:'186px', borderRadius: '43px'}} > </Btn>
                     </div>
            
                    </Grid>
     
                </AboutBackTop>
                <SecondCompo />
               
               <ThirdComponent/>
               <AboutBrand/>
               <Testimonials > 

               </Testimonials>

               <Footer
      style={{padding: '50px 0'}}
      >
        
      <div className="container">
        <div className="row">
          <div className="col-12 col-sm-6 col-md-3">
            <img src={logo}></img>
            
            <CreateTextElement
              element="p"
              fontSize= "12px"
              style={{lineHeight: '2.1', marginTop:'10px'}}
              >
                {footerString} 
            </CreateTextElement>
          </div>

          <div className="col-12 col-sm-6 col-md-3">
            <CreateTextElement
              element="h5"
              fontSize= "16px"
              style={{padding: '20px 5px 8px 5px'}}
              >
                Support
                
            </CreateTextElement>

            <CreateTextElement
            element='p'
            fontSize='13px'
            >
            <ul style={{listStyle:'none', padding: '0px 10px', lineHeight: '2.1'}}>
                  <li>Help Center</li>
                  <li>Get Started</li>
                  <li>Contact US</li>
            </ul>
            </CreateTextElement>

          </div>

          <div className="col-12 col-sm-6 col-md-3">
            <CreateTextElement
              element="h5"
              fontSize= "16px"
              style={{padding: '20px 5px 8px 5px'}}
              >
                About US
                
            </CreateTextElement>

            <CreateTextElement
            element='p'
            fontSize='13px'
            >
            <ul style={{listStyle:'none', padding: '0px 10px', lineHeight: '2.1'}}>
                  <li>About US</li>
                  <li>Terms of Use</li>
                  <li>Privacy Policy</li>
            </ul>
            </CreateTextElement>
          
          </div>

          <div className="col-12 col-sm-6 col-md-3">
            <CreateTextElement
              element="h5"
              fontSize= "16px"
              style={{padding: '20px 5px 8px 5px'}}
              >
                Get Newsletter
                
            </CreateTextElement>

            <TextboxFooter 
            style={{padding:'5px 0px 0px 10px'}}
            />
            <CreateTextElement
              
            >
            <i className="fas fa-globe" style={{padding:'0px 5px 10px 23px'}}></i>
            <i className="fab fa-facebook-f" style={{padding:'0px 20px'}}></i>
            <i className="fab fa-google-plus-g" style={{padding:'0px 8px'}}></i>
            <i className="fab fa-twitter" style={{padding:'0px 8px'}}></i>
            
            </CreateTextElement>
          </div> 
        </div>
        </div>
      

    </Footer>
            </div>
        )
    }
}

AboutusPage.defaultProps = {

}

AboutusPage.propTypes = {
    style: PropTypes.object
}

export default AboutusPage;