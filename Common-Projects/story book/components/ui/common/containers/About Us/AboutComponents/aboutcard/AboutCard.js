import React from 'react'
import PropTypes from 'prop-types'

class Aboutour extends React.Component{
    constructor(props){
        super(props)
    }

    backgroundStyle = {
        backgroundColor: '#0071bc',
        minHeight: '450px',
        position: 'relative'
    }

    // transLayer = {
    //     top: '0',
    //     left: '0',
    //     right: '0',
    //     bottom: '0',
    //     position: 'absolute',
    //     backgroundColor: 'rgba(0,0,0,'+this.props.opacity+')',
    //     padding: '70px 0'
    // }

    render(){
        return(
            <div style={{...this.backgroundStyle,...this.props.style}}>
                <div style={this.transLayer}>
                    <div className="container">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

Aboutour.defaultProps = {
    opacity: '.50'
}

Aboutour.propTypes = {
    style: PropTypes.object,
    backgroundImage: PropTypes.string,
    opacity: PropTypes.string
}

export default Aboutour;