import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid';
import Aboutmiddle1 from '../../About Us/aboutusmidle1componrnt';
import CreateTextElement from '../../../typography/create_text_element';
import {AboutString} from '../../../../../../components/constants/strings';
import aboutmdl1 from '../../../../../../assets/imgs/about.png';

class SecondCompo extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>
          
          <Aboutmiddle1 
                 
                 >
                       <Grid container spacing={12}>
                        <Grid item  md ={6} lg={6} xs={6} style={{zIndex: '1'}}>
                        <img  src={aboutmdl1} alt="aaa" style={{marginTop: '-45px',height: '453px',display: 'block',marginLeft: '-175px',width:'118%' , marginBottom: '-55px' }}/>
                     
                           </Grid>
                  <Grid item xs={6}  md ={6} lg={6}  style={{ marginTop: '55px',display: 'block'}}>  
                      
                <CreateTextElement
                  element="h5"
                  fontSize= "20px"
                  color = "black"
                  >
                    About 
                </CreateTextElement>
                
                <CreateTextElement
                  element="h4"
                  fontSize= "30px"
                  color = "#0071bc"
                  >
                   <strong>RND HUB</strong>
                </CreateTextElement>
        
                <CreateTextElement
                  element="p"
                  fontSize= "15px"
                  color = "black"
                  style={{lineHeight: '2.1', color:'black'}}
                  >
                    {AboutString}
                </CreateTextElement>
                  </Grid>
                </Grid>
       
                 
                    
                    
                 </Aboutmiddle1>
            </div>
        )
    }
}

SecondCompo.defaultProps = {

}

SecondCompo.propTypes = {
    style: PropTypes.object
}

export default SecondCompo;