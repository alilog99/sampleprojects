import React from 'react'
import PropTypes from 'prop-types'
import '../Navbar/Navbar.css';
import logo from '../../../../assets/imgs/logo.png';
import call from '../../../../assets/imgs/call sign.png';
import Button2 from '../button/button';
import ar from '../../../../assets/imgs/arrow.png';
import { Grid } from '@material-ui/core';
import menu from '../../../../assets/imgs/menu1.png';


class Navbar extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
          <Grid container spacing ={16}>           
          <Grid item xs={12} md={12} lg={12}> 
            <nav class="navbar navbar-expand-lg " style={{marginTop:'0'}}   >
             <img src={logo} alt="logo"  style={{marginTop: '11px'}} />
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" >
              <span class="navbar-toggler-icon"> <img src={menu} alt="icon"  style={{height:'38px'}}/></span>
            </button>
       
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
               <ul class="navbar-nav" style={{marginLeft: '-92px'}}>
                <li class="nav-item active">
                  <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Our Services</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Portfolio</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Contact US</a>
                </li>
                <li class="nav-item" >
                      <h6 style={{color:'white', marginTop: '11px',marginLeft: '0px'}}><img src={call} alt="call" />+1237771111</h6>
                </li>
                <li class="nav-item" style={{ paddingTop:'0px'}}>
                   <Button2 Gname="Get In Touch" imgarr={ar} style={{position: 'relative',minHeight: '100px',maxHeight: '123px',color:'white', backgroundColor:'#0071bc' , minHeight: '59px', width:'186px', borderRadius: '43px'}} />
                </li>
              </ul>
            </div>
        
     </nav>
    </Grid>
   </Grid>

        )
    }
}

Navbar.defaultProps = {

}

Navbar.propTypes = {
    style: PropTypes.object
}

export default Navbar;