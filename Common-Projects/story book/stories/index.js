import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Button } from '@storybook/react/demo';
import 'bootstrap/dist/css/bootstrap.css'
import Aboutus from '../components/ui/common/containers/About Us/aboutuspge';
import Background1 from '../components/ui/common/containers/Home page componentts/background';
import TextboxFooter from '../components/ui/etc/footer text/textbox';
import Topback from '../components/ui/common/containers/Home page componentts/topback';
import CardsCompo from '../components/ui/common/containers/Home page componentts/cardsCompo';
import FooterCompo from '../components/ui/common/containers/Home page componentts/FooterCompo';
import Cards from '../components/ui/etc/Card';
import Slide from '../components/ui/etc/text carosal/slider'; 
import FirstCompo from '../components/ui/common/containers/About Us/AboutComponents/firstComponent';
import Nav from '../components/ui/common/Navbar/navbar';
import SecondCompo from '../components/ui/common/containers/About Us/AboutComponents/SecondCompo';
import Testimonials from '../components/ui/etc/testimonial/Testimonial';
import MobileImg from '../components/ui/common/containers/Home page componentts/mobileimgs';
import Home3Com from '../components/ui/common/containers/Home page componentts/Home3Com';
import ThirdComponent from '../components/ui/common/containers/About Us/AboutComponents/thirdComponent';
require('../assets/css/main.css')


storiesOf('Individual Components', module)
  .add('with text', () => (
    <Button onClick={action('clicked')}>Hello Button</Button>
  ))
  .add('TextboxFooter', () => (
    <TextboxFooter />
    ))
    .add('card', () => (
      <Cards />
      ))
      .add('slider', () => (
       <Slide />
        ))
        .add('Navbar', () => (
         <Nav   />
           ))
   


storiesOf('HomeContainers', module)
  .add('Home Page ', () => (
  <Background1 />
  ))
  
  .add('top background', () => (
    <Topback />
    ))
    .add('Card components', () => (
      <CardsCompo />
      ))
      .add('Testimonial', () => (
        <Testimonials />
        ))
      .add('Footer Comonents', () => (
        <FooterCompo />
        ))     
        .add('Mobile Img Compo', () => (
          <MobileImg />
          ))     
          .add('Home About Comp', () => (
             <Home3Com />
            ))    

  storiesOf('aboutContainers', module)
   .add('about Us Page', () => (
  <Aboutus />
  ))
  .add('First Components', () => (
    <FirstCompo />
    ))
    .add('Second Components', () => (
      <SecondCompo />
      ))
         
      .add('Third Components', () => (
        <ThirdComponent />
        ))
           


