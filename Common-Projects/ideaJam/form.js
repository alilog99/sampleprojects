

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField'; 
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
});

const currencies = [
  {
    value: 'USD',
    label: '$',
  },
  {
    value: 'EUR',
    label: '€',
  },
  {
    value: 'BTC',
    label: '฿',
  },
  {
    value: 'JPY',
    label: '¥',
  },
];

class TextFields extends React.Component {
  state = {
    placeholder: 'Enter your ClientName',
    age: '',
    multiline: 'Controlled',
    currency: 'EUR',
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <form className={classes.container} noValidate autoComplete="off">
       
<div className="Client">

<div className="Client" style={{width: "100%"}}></div>
<h2>Client</h2>
<Grid container spacing ={8}>     
            <Grid item xs={6} sm = {4} md = {3}>
            <TextField
          
          id="standard-error"
          label="Client-Name*"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        />
            </Grid>

            <Grid item xs={6} sm = {4} md = {3}>
            <TextField
          
          id="standard-error"
          label="Client-Market*"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        />
            </Grid>


            <Grid item xs={6} sm = {4} md = {3}> 
            <TextField
          
          id="standard-error"
          label="Client-Brand*"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        />
            </Grid>

            <Grid item xs={6} sm = {4} md = {3}>
             <TextField
          
          id="standard-error"
          label="Client-industry*"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        />
            </Grid>
            </Grid>

</div>
<div style={{marginleft:'auto'}}></div>
        <div className="brief" style={{width: "100%"}}>
        <h2>Brief</h2>
            <Grid container spacing ={8}>     
            <Grid item xs={6} sm = {4} md = {3}>
             <TextField
          
          id="standard-error"
          label="Brief - Category *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        />
            </Grid>
            <Grid item xs={6} sm = {4} md = {3}>
            <TextField
          
          id="standard-error"
          label="Brief - Keywords *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        /></Grid>
            <Grid item xs={6} sm = {4} md = {3}>
            <TextField
          
          id="standard-error"
          label="Brief - Description *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        /></Grid>
          <Grid item xs={6} sm = {4} md = {3}>
          <TextField
          
          id="standard-error"
          label="Brief - Client goals *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        /></Grid>
        <Grid item xs={6} sm = {4} md = {3}>
        <TextField
          
          id="standard-error"
          label="Brief - Insights & Strategy *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        /></Grid>  
        <Grid item xs={6} sm = {4} md = {3}>
        <TextField
          
          id="standard-error"
          label="Brief - Challenges *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        /></Grid>     
        <Grid item xs={6} sm = {4} md = {3}>
        <TextField
          
          id="standard-error"
          label="Brief - Client testimonial  *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        /></Grid>                        
            </Grid>
</div>
<Grid container className={classes.root} spacing={8}></Grid>
           <div className="Campaign">
           <h2>Compaign</h2>
            <Grid container spacing ={8}>
            <Grid item xs= {6} sm = {4} md = {3}>
            <TextField
           id="standard-error"
          label="Campaign - Summary *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
                                />
          </Grid>

          <Grid item xs={6} sm = {4} md = {3}>
           <TextField
          id="standard-error"
          label="Campaign - Dates *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
                                />
          </Grid>

          <Grid item xs={6} sm = {4} md = {3}> 
          <TextField
          id="standard-error"
          label="Campaign - Media *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
                                />
          </Grid>

          <Grid item xs={6} sm = {4} md = {3}> 
          <TextField
          id="standard-error"
          label="Campaign - Awards*"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
                                />
          </Grid>

          <Grid item xs={6} sm = {4} md = {3}> 
          <TextField
           
           id="standard-error"
          label="Campaign - Creative *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
                                />
          </Grid>
           </Grid>
    </div>
    
    <div className="Client">
    <h2>Team</h2>
    <Grid container className={classes.root} spacing={10}>  
            <Grid item xs={6} sm = {4} md = {3}>
                       <TextField
          id="standard-error"
          label="Case study - author *"
          placeholder="placeholder"
          className={classes.textField}
          margin="normal"
        />
            </Grid>

            <Grid item xs={6} sm = {4} md = {3}>
                      <TextField
         id="standard-error"
         label="Team*"
         placeholder="placeholder"
         className={classes.textField}
         margin="normal"
       />
           </Grid>

           <Grid item xs={6} sm = {4} md = {3}>
                    <TextField
         id="standard-error"
         label="Agency *"
         placeholder="placeholder"
         className={classes.textField}
         margin="normal"
       />
           </Grid>

           <Grid item xs={6} sm = {4} md = {3}>
                  <TextField
         id="standard-error"
         label="Market *"
         placeholder="placeholder"
         className={classes.textField}
         margin="normal"
       />
           </Grid>
           </Grid>
          

          
            </div>
        
      </form>
    );
  }
}

TextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TextFields);


