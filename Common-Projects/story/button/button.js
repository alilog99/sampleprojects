import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'
 
const styles = {
    butt: {
      width: '115px',
     
      position: 'relative',
      minHeight: '100px',
      margin: '15px',
      maxHeight: '123px'
      
    },
    
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
  };

class Button1 extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        const {classes} = this.props;
        return(
            <div >

      <Button  className={classes.butt} style={{color:'black' , backgroundColor:'#0071bc' ,  minHeight: '47px',width:'150px',borderRadius: '43px'}}>
        Get Started
      </Button>
     
     
      
  
            </div>
        )
    }
}

Button1.defaultProps = {

}

Button1.propTypes = {
    style: PropTypes.object
}

export default  withStyles(styles)(Button1);