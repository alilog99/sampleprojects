import React from 'react'
import PropTypes from 'prop-types'


import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';


const styles = theme => ({
  container: {
   
    position: 'relative',
    // marginLeft:'700px',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '70%',
    margin: '10px',
    marginTop:'40px',
  },
  dense: {
    marginTop: 16,
  },
  
});

const ranges = [
    {
      value: 'Service 1',
      label: 'Service 2',
    },
    {
      value: 'Service 3',
      label: 'Service 4',
    },
    {
      value: 'Service 5',
      label: 'Service 6',
    },
  ];

class OutlinedTextFields extends React.Component{
    constructor(props){
        super(props)

    }
    state = {
        name: 'Cat in the Hat',
        age: '',
        multiline: 'Controlled',
       
      };
    
      handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
      };
    render(){
        const { classes } = this.props;
        return(
            <div style={{...this.props.style}}>
    
      <form className={classes.container} noValidate autoComplete="off">
      <Grid container spacing={12}>
        <Grid item md={6} >
      <TextField style={{backgroundColor:'white',marginTop:'15px'}}
        id="outlined-Name-input"
        label="Your Name"
        className={classes.textField}
        type="Name"
        name="Name"
        autoComplete="Name"
        margin="normal"
        variant="outlined"
      />
      </Grid>
      <Grid item md={6} >
      <TextField style={{backgroundColor:'white',marginTop:'15px'}}
        id="outlined-email-input"
        label="Your Email"
        className={classes.textField}
        type="email"
        name="email"
        autoComplete="email"
        margin="normal"
        variant="outlined"
      />
      </Grid>
      <Grid item md={6} >
      <TextField style={{backgroundColor:'white',marginTop:'15px'}}
        id="Phone Number"
        label="Phone Number"
        className={classes.textField}
        type="Phone Numberl"
        name="Phone Number"
        autoComplete="Phone Number"
        margin="normal"
        variant="outlined"
      />
      </Grid>
      <Grid item md={6} >
      <TextField style={{backgroundColor:'white',marginTop:'15px'}}
          select 
          className={classNames( classes.textField)}
          variant="filled"
          label ="Select Services"
          value={this.state.weightRange}
          onChange={this.handleChange('weightRange')}
          InputProps={{
            startAdornment: (
              <InputAdornment variant="filled" position="start">
                service
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      </Grid>
    </form>
  

            </div>
        )
    }
}

OutlinedTextFields.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(OutlinedTextFields);