import React from 'react'
import PropTypes from 'prop-types'
import Antman from '../../../../../assets/imgs/antman.png';
import Comp from  '../../../../../assets/imgs/computer.png';
import Btn from '../../button/button';
import ar from '../../../../../assets/imgs/arrow.png';
import Layer from '../../../../../assets/imgs/Layer34.png';
import Layer1 from '../../../../../assets/imgs/Layer35.png';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import  Text  from  '@material-ui/core/TextField';

import CreateTextElement from '../../typography/create_text_element';

class Services extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div>

    <Grid container spacing={12}>
         <Grid item md={6} style={{backgroundColor:'#0071bc',width:'100%'}}>

              <CreateTextElement
                 element="h5"
                 fontSize= "20px"
              >
                  <h4 style={{marginTop:'100px',marginLeft:'100px'}}>We Offer</h4> 
                  <h3 style={{marginLeft:'100px'}}>Mobile App Development</h3>
                  <h6 style={{marginLeft:'100px',lineHeight:'40px'}}>Ut a elit diam.Morbi ut mauris eleifend, iaculis mi id, pellentesque mi,Suspendisse gravida tortor ac ante condimentum scelerisque eu eu sem.Suspendisse potenti.Morbi vitae venenatis purus.Nam quis faucibus dui.Curabitur congue eros consequat, in pharetra mi ornare.Aenean non lorem varius leo scelerisque tempor suscipit</h6>
              
              </CreateTextElement>
              <Btn
                Gname="Get Started"   imgarr={ar} style={{position: 'relative',minHeight: '100px',maxHeight: '123px',color:'black', backgroundColor:'white' , minHeight: '59px', width:'186px', borderRadius: '43px',marginTop: '4px',marginLeft: '100px'}}/>
             
              
        
           </Grid>
    <Grid item md={6} >
            < img style={{width:'100%',height:'570px'}} src={Antman} alt='ant'/>
    </Grid>
  <Grid item md={6}>
            <img style={{width:'100%',height:'650px'}} src={Comp} alt='comp'/>
  </Grid>
     <Grid item md={6}style={{backgroundColor:'#8bc53f',width:'100%',color:'white'}}>
                 <h4 style={{margin:'20px',marginTop:'100px'}}>We Offer</h4> 
                 <h3 style={{margin:'20px'}}>WebSite Development</h3>
                 <h6 style={{margin:'20px',lineHeight:'40px'}}>Ut a elit diam.Morbi ut mauris eleifend, iaculis mi id, pellentesque mi,Suspendisse gravida tortor ac ante condimentum scelerisque eu eu sem.Suspendisse potenti.Morbi vitae venenatis purus.Nam quis faucibus dui.Curabitur congue eros consequat, in pharetra mi ornare.Aenean non lorem varius leo scelerisque tempor suscipit</h6>
              
                 <Btn
                    Gname="Get Started"   imgarr={ar} style={{position: 'relative',minHeight: '100px',maxHeight: '123px',color:'black', backgroundColor:'#0071bc' , minHeight: '59px', width:'186px', borderRadius: '43px',marginTop: '4px',marginLeft: '10px'}}/>
                
    </Grid>
    <Grid item md={6}style={{backgroundColor:'#deb20f',width:'100%',color:'white'}}>
                 <h4 style={{marginTop:'100px',marginLeft:'100px'}}>We Offer</h4> 
                 <h3 style={{marginLeft:'100px'}}>Game Development</h3>
                 <h6 style={{marginLeft:'100px',lineHeight:'40px'}}>Ut a elit diam.Morbi ut mauris eleifend, iaculis mi id, pellentesque mi,Suspendisse gravida tortor ac ante condimentum scelerisque eu eu sem.Suspendisse potenti.Morbi vitae venenatis purus.Nam quis faucibus dui.Curabitur congue eros consequat, in pharetra mi ornare.Aenean non lorem varius leo scelerisque tempor suscipit</h6>
              
                 <Btn
                    Gname="Get Started"   imgarr={ar} style={{position: 'relative',minHeight: '100px',maxHeight: '123px',color:'black', backgroundColor:'white' , minHeight: '59px', width:'186px', borderRadius: '43px',marginTop: '4px',marginLeft: '100px'}}/>
                
    </Grid>
    <Grid item md={6} >
            < img style={{width:'100%',height:'570px'}} src={Layer} alt='ant'/>
    </Grid>
    <Grid item md={6} >
            < img style={{width:'100%',height:'570px'}} src={Layer1} alt='ant'/>
    </Grid>
    <Grid item md={6}style={{backgroundColor:'#8e2bb9',width:'100%',color:'white'}}>
                 <h4 style={{margin:'20px',marginTop:'100px'}}>We Offer</h4> 
                 <h3 style={{margin:'20px'}}>Digital Marketing</h3>
                 <h6 style={{margin:'20px',lineHeight:'40px'}}>Ut a elit diam.Morbi ut mauris eleifend, iaculis mi id, pellentesque mi,Suspendisse gravida tortor ac ante condimentum scelerisque eu eu sem.Suspendisse potenti.Morbi vitae venenatis purus.Nam quis faucibus dui.Curabitur congue eros consequat, in pharetra mi ornare.Aenean non lorem varius leo scelerisque tempor suscipit</h6>
              
                 <Btn
                    Gname="Get Started"   imgarr={ar} style={{position: 'relative',minHeight: '100px',maxHeight: '123px',color:'black', backgroundColor:'#0071bc' , minHeight: '59px', width:'186px', borderRadius: '43px',marginTop: '4px',marginLeft: '10px'}}/>
    </Grid>
    
    <Grid item md={12} style={{backgroundColor:'#0071bc',color:'white'}}>
    <h4 style={{marginTop:'100px',marginLeft:'100px'}}>Request A Call Back</h4> 
                  <h5 style={{marginLeft:'100px'}}>Call To Action</h5>
                  <h6 style={{marginLeft:'100px',fontSize:'10px',color:'white'}}>Ut a elit diam.Morbi ut mauris eleifend, iaculis mi id, pellentesque mi,<br></br>Suspendisse gravida tortor ac ante scelerisque eu eu sem.Suspendisse potenti.</h6>
                  
    </Grid>
    </Grid>
     </div>
        )
    }
}

Services.defaultProps = {

}

Services.propTypes = {
    style: PropTypes.object
}

export default Services;