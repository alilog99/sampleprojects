import React from 'react'
import PropTypes from 'prop-types'
import Card from '../../etc/Card';
import App from '../../../../assets/imgs/app_dev.png';
import web from '../../../../assets/imgs/web_dev.png';
import digi from '../../../../assets/imgs/digital_mon.png';
import game from '../../../../assets/imgs/gamedevicon.png';
import vrr from '../../../../assets/imgs/vrappicon.png';
import dig from '../../../../assets/imgs/digmarkicon.png';
import Button1 from '../button/button';
import { bannerString } from '../../../constants/strings';
import bannerImg from '../../../../assets/imgs/banner.png';
import BackgroundContainer from '../containers/background_container';
import CreateTextElement from '../../../ui/common/typography/create_text_element';
import Grid from '@material-ui/core/Grid';
import ar from '../../../../assets/imgs/arrow.png';

class Background1 extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <BackgroundContainer
            backgroundImage = {bannerImg}
            opacity = ".95"
            style={{padding: '150px 0'}}
            > 
      
            
            <Grid container spacing={16}>
              <Grid item xs={6} >
                <Grid item xs={12} lg={12} md={12} >
      
              <CreateTextElement
                element="h5"
                fontSize= "20px"
                >
                  Thriving To Achieve Smart, 
              </CreateTextElement>
              
              <CreateTextElement
                element="h4"
                fontSize= "30px"
                >
                  Unique & State Of The Art Solutions
              </CreateTextElement>
      
              <CreateTextElement
                element="p"
                fontSize= "15px"
                style={{lineHeight: '2.1'}}
                >
                  {bannerString}
              </CreateTextElement>
              </Grid>
              </Grid>
              <Grid item xs={6} >  
                 <Grid item xs={12} lg={12} md={12}>
                <Card   Imgsd={App} Tname="app development" />
                <Card   Imgsd={web} Tname="Web development"/>
                <Card   Imgsd={digi} Tname="Digital marketing"/>
                <Card   Imgsd={game} Tname="game development"/>
                <Card   Imgsd={vrr} Tname="VR Application"/>
                <Card   Imgsd={dig} Tname="Digital Marketing"/>
                </Grid>
              </Grid>
              
             <Grid item xs={6} >
             <Grid item xs={12} lg={12} md={12} >
              <Button1 Gname="Get Started" imgarr={ar}style={{position: 'relative',minHeight: '100px',maxHeight: '123px',color:'white', backgroundColor:'#0071bc' , minHeight: '59px', width:'186px', borderRadius: '43px',marginTop: '-69px',marginLeft: '1px'}} />
             </Grid>
             </Grid>
            </Grid>
            
      
          </BackgroundContainer>
        )
    }
}

Background1.defaultProps = {

}

Background1.propTypes = {
    style: PropTypes.object
}

export default Background1;