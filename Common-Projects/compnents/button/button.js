import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import arr from '../../../../assets/imgs/arrow.png';
import CreateTextElemen from '../typography/create_text_element';
 
    
    

class Button1 extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
       
        return(
            <div >

      <Button  className="buton"style={{...this.props.style}} >
      <CreateTextElemen
          element="h6"
          fontSize= "14px"
          style={{marginTop: '4px'}}
          
          >
          
           {this.props.Gname}
           <img src={this.props.imgarr} alt ="arrow"  style={{marginLeft: '3px'}}/>
        </CreateTextElemen>
  
      </Button>
     
     
      
  
            </div>
        )
    }
}

Button1.defaultProps = {

}

Button1.propTypes = {
    style: PropTypes.object
}

export default  (Button1);