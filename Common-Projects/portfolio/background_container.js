
import React from 'react'
import PropTypes from 'prop-types'

class Background extends React.Component{
    constructor(props){
        super(props)
    }

    backgroundStyle = {
        background: 'url('+this.props.backgroundImage+')',
       
        
    }

    

    render(){
        return(
            <div style={{...this.backgroundStyle,...this.props.style}}>
                <div style={this.transLayer}></div>
                <div className="container" style={{position:'relative'}}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

Background.defaultProps = {
    opacity: '.50'
}

Background.propTypes = {
    style: PropTypes.object,
    backgroundImage: PropTypes.string,
    opacity: PropTypes.string
}

export default Background;

