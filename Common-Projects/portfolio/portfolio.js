import React from 'react'
import PropTypes from 'prop-types'
import  OurServ1 from '../portfolio/OurSer1';
import CenteredTabs  from '../portfolio/tabsbar';
import PortGrid from '../portfolio/portfoliogrid'
import Back from '../../../../../assets/imgs/portfolioimg/image1.png';
import Mob from '../../../../../assets/imgs/portfolioimg/image2.png';
import Lap from '../../../../../assets/imgs/portfolioimg/image3.png';
import Lap1 from '../../../../../assets/imgs/portfolioimg/image4.png';
import Smart from '../../../../../assets/imgs/portfolioimg/image5.png';
import Smart1 from '../../../../../assets/imgs/portfolioimg/image6.png';
import FooterCompo from '../our services/footercom';

class Portfolio extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>
            <OurServ1 />

            <CenteredTabs />
            <div style={{textAlign: 'center', paddingTop: '16px'}}>
                <PortGrid title="ADMIRALTY H-NOTE" backImg = {Back}/>
                <PortGrid title="Hawkins" backImg = {Mob}/>
                <PortGrid title="Hamshire" backImg = {Lap}/>
                <PortGrid title="Accu-Mile" backImg = {Lap1}/>
                <PortGrid title="Q2" backImg = {Smart}/>
                <PortGrid title="Discover-Varmints" backImg = {Smart1}/>
            </div>
            <FooterCompo />
            </div>

        )
    }
}

Portfolio.defaultProps = {

}

Portfolio.propTypes = {
    style: PropTypes.object
}

export default Portfolio;