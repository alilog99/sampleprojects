import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid';
import Back from '../../../../../assets/imgs/portfolioimg/image1.png';
import Mob from '../../../../../assets/imgs/portfolioimg/image2.png';
import Lap from '../../../../../assets/imgs/portfolioimg/image3.png';
import Lap1 from '../../../../../assets/imgs/portfolioimg/image4.png';
import Smart from '../../../../../assets/imgs/portfolioimg/image5.png';
import Smart1 from '../../../../../assets/imgs/portfolioimg/image6.png';
import Background from '../portfolio/background_container';
import CreateTextElement from '../../typography/create_text_element';
class PortGrid extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{display: 'inline-block',...this.props.style, margin: '16px'}}>
                <img style={{width: '300px'}} src={this.props.backImg}  />
                
                <CreateTextElement
                    element="div"
                    color= 'black'
                    style={{textAlign:'center'}}
                    >
                    {this.props.title}
                    </CreateTextElement>
            </div>

            
        )
    }
}

PortGrid.defaultProps = {

}

PortGrid.propTypes = {
    style: PropTypes.object
}

export default PortGrid;