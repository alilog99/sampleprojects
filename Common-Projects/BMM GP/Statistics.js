import React from 'react';
import PropTypes from 'prop-types';
import SimplePanel from '../panel/simple_panel'
import CreateTextElement from '../../common/typography/element_white'
import User from '../../../../../superadmin/slices/Customer.png';
import Garage from '../../../../../superadmin/slices/Garage.png';
import Companies from '../../../../../superadmin/slices/company1.png';
import Earning from '../../../../../superadmin/slices/earning.png';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
const styles = theme => ({
    wrapper: {
      width: '24%'
    },
    paper: {
        width:'100%',
        height:'90%',
        margin:'21px 0',
        padding: '11px',
        border: '1px solid green'
      
    },
    
  });


class SiteStatistics extends React.Component{
    constructor(props){
        super(props)
    }
  
    render(){
        const { classes } = this.props;
        return(
            <div style={this.props.style}>
                <SimplePanel
                style={{minHeight:'374px',width:'auto',marginTop:'10px'}}
                backgroundColor='#ce0101'
                title='Site Statistics'
                >
            <div spacing={12}>
            <div className="row">
            <div className="col-lg-6">
            <Paper style={{width:'105%',border:'1px solid purple'}} className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{width:'40%',marginTop: '10px'}} >
                        <Grid item  style = {{alignItems: 'center'}}>
                            <img src={User}/>
                        </Grid>
                        <Grid item xs={4} >
                      <CreateTextElement 
                          element="h5" 
                          fontSize="18px"
                          color="#777777"
                          style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                34
                      </CreateTextElement>
                      <CreateTextElement 
                          element="p" 
                          fontSize="15px"
                          color="777777"
                          style={{margin:'0'}}
                      >
                               Customer
                      </CreateTextElement>
               </Grid>
               </Grid>
               </Paper>
               </div>
               <div className="col-lg-6">
               <Paper style={{width:'105%',border:'1px solid orange'}} className={classes.paper}>
                        <Grid container wrap="nowrap" spacing={16}style={{width:'40%',marginTop: '10px'}} >
                        <Grid item  style = {{alignItems: 'center'}}>
                            <img src={Garage}/>
                        </Grid>
                        <Grid item xs={4}>
                      <CreateTextElement 
                          element="h5" 
                          fontSize="18px"
                          color="#777777"
                          style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                14
                      </CreateTextElement>
                      <CreateTextElement 
                          element="p" 
                          fontSize="15px"
                          color="777777"
                          style={{margin:'0'}}
                      >
                               Garage
                      </CreateTextElement>
               </Grid>
               </Grid>
               </Paper>
               </div>
               </div>
               </div>
            <div spacing={12}>
            <div className="row">
            <div className="col-lg-6">
                <Paper style={{width:'105%',border:'1px solid skyblue'}} className={classes.paper}>
                <Grid container wrap="nowrap" spacing={16}style={{width:'40%',marginTop: '10px'}} >
                        <Grid item  style = {{alignItems: 'center'}}>
                            <img src={Companies}/>
                </Grid>
                <Grid item xs={4}>
                    <CreateTextElement 
                        element="h5" 
                        fontSize="18px"
                        color="#777777"
                        style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                75
                    </CreateTextElement>
              <CreateTextElement 
                  element="p" 
                  fontSize="15px"
                  color="777777"
                  style={{margin:'0',width:'118px'}}
              >
                       Companies
              </CreateTextElement>
            </Grid>
            </Grid>
            </Paper>
        </div>
       <div className="col-lg-6">
       <Paper style={{width:'105%',border:'1px solid blue'}} className={classes.paper}>
                <Grid container wrap="nowrap" spacing={16}style={{width:'40%',marginTop: '10px'}} >
                <Grid item  style = {{alignItems: 'center'}}>
                    <img src={Earning}/>
                </Grid>
                <Grid item xs={4} >
              <CreateTextElement 
                  element="h5" 
                  fontSize="15px"
                  color="#777777"
                  style={{marginBottom:'6px', flex: '1 0 100%'}}>
                        $700
              </CreateTextElement>
              <CreateTextElement 
                  element="p" 
                  fontSize="15px"
                  color="777777"
                  style={{margin:'0',width:'118px'}}
              >
                       Earning
              </CreateTextElement>
       </Grid>
       </Grid>
       </Paper>
       </div>
       </div>
       </div>
            
                </SimplePanel>   
            </div>
      
        )
    }

} 
  
SiteStatistics.defaultProps = {
    classes: PropTypes.object.isRequired,
}

SiteStatistics.propTypes = {
    style: PropTypes.object
}

export default withStyles(styles) (SiteStatistics);