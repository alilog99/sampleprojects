import React from 'react';
import PropTypes from 'prop-types';
import SiteStatistics from './Statistics'
import BookingStatistics from './Booking'

class BookingSite extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
          
            <div  className="row"spacing={12}>
            <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            
                <SiteStatistics
                >
                </SiteStatistics>
                </div>
                <div className="col-12 col-sm-6 col-md-6 col-lg-6">
               
                <BookingStatistics
                >
                </BookingStatistics>
            
            </div>
            
            </div>
            

            
           
        )
    }

}

BookingSite.defaultProps = {

}

BookingSite.propTypes = {
    style: PropTypes.object
}

export default BookingSite;