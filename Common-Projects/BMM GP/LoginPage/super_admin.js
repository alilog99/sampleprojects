import React from 'react';
import PropTypes from 'prop-types';
import backgroundImage from '../../../../../assets/imgs/login_1.png'
import GrayBackgroundContainer from '../../containers/gray_container/gray_background_container'
import CreateTextElement from '../../../common/typography/element_white'
import InputIcon from '../../../common/inputs/input_with_icon/input_icon'
import ButtonTemplate from '../../../common/buttons/button_template'
import AnchorText from '../../../common/typography/anchor_text'
import {darkBlue, themeRed} from '../../../../constants/colors'
import SmFooter from '../../footer/sm_footer';



class SuperAdminLoginPage extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <React.Fragment>
            <GrayBackgroundContainer
                coverImage = {backgroundImage}
                style={{height: 'calc(100vh - 30px)'}}
                >
                <div>
                    <div style={{margin: '33px'}} className="d-none d-sm-none d-md-block"></div>
                    <div className="text-center" style={{marginBottom: '22px'}}>
                   </div>
                   <div>
                    <CreateTextElement
                        element = "h3"
                        color = {darkBlue}
                        fontSize = "22px"
                        style={{marginBottom: '22px', fontWeight: 'bold'}}
                        >
                        SUPER ADMIN
                    </CreateTextElement>
                    </div>
                    <div
                        style={{marginBottom: '10px'}}
                        >
                        <InputIcon
                           inputIcon = "fas fa-user"
                            inputType='text'
                            iconColor = {themeRed}
                            placeholder="Username"
                            />
                    </div>
                    <div
                        style={{marginBottom: '30px'}}
                        >
                        <InputIcon
                            inputIcon = "fas fa-lock"
                            inputType='password'
                            placeholder = 'Password'
                            iconColor = {themeRed}
                            />
                    </div>
                    <ButtonTemplate
                    template="red"
                        style={{width: '100%', height: '45px'}}
                        >
                        LOGIN
                    </ButtonTemplate>
                    <div>
                    <AnchorText
                        href="#"
                        style={{margin: '25px 0', textAlign:'center',fontSize:'12px'}}
                        >
                        Forgot Password?
                    </AnchorText>
                    </div>
                    <ButtonTemplate
                        template="dark"
                        style={{width: '100%', height: '45px'}}
                        >
                        SIGNUP
                    </ButtonTemplate>
                </div>
            </GrayBackgroundContainer>
            <SmFooter style={{position:'fixed',bottom: '0',left: '0',right: '0'}}>
                &copy; Book MOT Slot, {new Date().getFullYear()}, All rights reserved!
            </SmFooter>
            </React.Fragment>
        )
    }

}

SuperAdminLoginPage.defaultProps = {

}

SuperAdminLoginPage.propTypes = {
    style: PropTypes.object
}

export default SuperAdminLoginPage;