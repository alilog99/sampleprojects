import React from 'react';
import PropTypes from 'prop-types';
import SimplePanel from '../panel/simple_panel'
import CreateTextElement from '../../common/typography/element_white'
import Booking from '../../../../../superadmin/slices/total_booking.png';
import CancelBooking from '../../../../../superadmin/slices/Cancel_booking.png';
import Complete from '../../../../../superadmin/slices/Completed.png';
import Pending from '../../../../../superadmin/slices/Pending.png';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
const styles = theme => ({
    wrapper: {
      width: '26%'
    },
    paper: {
        width:'100%',
        height:'90%',
        margin:'21px 0',
        padding: '12px',
      
    },
    
  });


class BookingStatistics extends React.Component{
    constructor(props){
        super(props)
    }
  
    render(){
        const { classes } = this.props;
        return(
            <div style={this.props.style}>
                <SimplePanel
                style={{minHeight:'374px',width:'auto',marginTop:'10px'}}
                backgroundColor='#ce0101'
                title='Booking Statistics'
                >
            <div spacing={12}>
            <div className="row">
            <div className="col-lg-6">
            <Paper style={{width:'105%',border:'1px solid purple'}} className={classes.paper}>
            <Grid container wrap="nowrap" spacing={16}style={{width:'40%',marginTop: '10px'}} >
                        <Grid item  style = {{alignItems: 'center'}}>
                            <img src={Booking}/>
                        </Grid>
                        <Grid item xs={4} >
                      <CreateTextElement 
                          element="h5" 
                          fontSize="14px"
                          color="#777777"
                          style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                100
                      </CreateTextElement>
                      <CreateTextElement 
                          element="p" 
                          fontSize="15px"
                          color="777777"
                          style={{margin:'0',width:'100px'}}
                      >
                               Total Booking
                      </CreateTextElement>
               </Grid>
               </Grid>
               </Paper>
               </div>
               <div className="col-lg-6">
               <Paper style={{width:'105%',border:'1px solid orange'}} className={classes.paper}>
                        <Grid container wrap="nowrap" spacing={16}style={{width:'40%',marginTop: '10px'}} >
                        <Grid item  style = {{alignItems: 'center'}}>
                            <img src={CancelBooking}/>
                        </Grid>
                        <Grid item xs={4} >
                      <CreateTextElement 
                          element="h5" 
                          fontSize="14px"
                          color="#777777"
                          style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                100
                      </CreateTextElement>
                      <CreateTextElement 
                          element="p" 
                          fontSize="15px"
                          color="777777"
                          style={{margin:'0',width:'118px'}}
                      >
                               Cancel Booking
                      </CreateTextElement>
               </Grid>
               </Grid>
               </Paper>
               </div>
               </div>
             
            
            <div className="row">
            <div className="col-lg-6">
                <Paper style={{width:'105%',border:'1px solid skyblue'}} className={classes.paper}>
                <Grid container wrap="nowrap" spacing={16}style={{width:'40%',marginTop: '10px'}} >
                        <Grid item  style = {{alignItems: 'center'}}>
                            <img src={Complete}/>
                </Grid>
                <Grid item xs={4} >
                    <CreateTextElement 
                        element="h5" 
                        fontSize="18px"
                        color="#777777"
                        style={{marginBottom:'6px', flex: '1 0 100%'}}>
                                75
                    </CreateTextElement>
              <CreateTextElement 
                  element="p" 
                  fontSize="15px"
                  color="777777"
                  style={{margin:'0'}}
              >
                       Complete
              </CreateTextElement>
            </Grid>
            </Grid>
            </Paper>
        </div>
       <div className="col-lg-6">
       <Paper style={{width:'105%',border:'1px solid blue'}} className={classes.paper}>
                <Grid container wrap="nowrap" spacing={16}style={{width:'40%',marginTop: '10px'}} >
                <Grid item  style = {{alignItems: 'center'}}>
                    <img src={Pending}/>
                </Grid>
                <Grid item xs={4}>
              <CreateTextElement 
                  element="h5" 
                  fontSize="18px"
                  color="#777777"
                  style={{marginBottom:'6px', flex: '1 0 100%'}}>
                        15
              </CreateTextElement>
              <CreateTextElement 
                  element="p" 
                  fontSize="15px"
                  color="777777"
                  style={{margin:'0'}}
              >
                       Pending
              </CreateTextElement>
       </Grid>
       </Grid>
       </Paper>
       </div>
       </div>
       </div>
            
                </SimplePanel>   
            </div>
      
        )
    }

} 
  
BookingStatistics.defaultProps = {
    classes: PropTypes.object.isRequired,
}

BookingStatistics.propTypes = {
    style: PropTypes.object
}

export default withStyles(styles) (BookingStatistics);