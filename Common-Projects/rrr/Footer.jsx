import React, { Component } from 'react';
import {Grid} from 'react-bootstrap'; 
import Logo from '../assets/logo.png';
import {Link , Route} from 'react-router-dom';
import '../components/Footer.css';
import Text from '../components/Textfield';
import Button from '@material-ui/core/Button';
import aar from '../assets/10.png';
import dd from '../assets/drib.png';
import FB from '../assets/fb.png';
import go from '../assets/gog.png';
import twi from '../assets/tw.png';


class Footer extends Component {
    render() {
        
        return (
            <div>
                
                  <Grid className="logo">
                   <img src={Logo} alt="logo"/>
                  </Grid>    
                  <Grid className="support">       
                  <h6> Support</h6>
                  </Grid>
                <Grid  className="help">
                  <Link  to="Contact">
                  <h6>Help Center</h6>
                  </Link>
                  </Grid>
                  <Grid  className="help">
                  <Link  to="Contact">
                  <h6>Get Started</h6>
                  </Link>
                  <Link  to="Contact">
                  <h6>Contact Us</h6>
                  </Link>
                  </Grid>
              <p>  Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. USA &amp; CAN:1-888-123-4567<br/> Address: 34 Brokel Rd.NY </p>
                  <Grid className="about">
                      <h6>About US</h6>
                      </Grid>     
                 <Grid  className="aboutus">
                         <Link  to="About">
                           <h6>About Us</h6>
                              </Link>
                             
                   </Grid>      
                   <Grid className="term">
                   <Link  to="About">
                           <h6>Term of Use</h6>
                              </Link>
                       </Grid>     
                       <Grid className="privacy">
                   <Link  to="About">
                           <h6>Privacy Policy</h6>
                              </Link>
                       </Grid>     
                      <Grid className="Textfield">
                        <img src={dd} alt="drib" style={{  float: 'right',marginTop: -85,marginRight: 378}}/>
                        <img src={FB} alt="drib" style={{  float: 'right',marginTop: -85,marginRight: 339}}/>
                        <img src={go} alt="drib" style={{  float: 'right',marginTop: -85,marginRight: 287}}/>
                        <img src={twi} alt="drib" style={{  float: 'right',marginTop: -85,marginRight: 242}}/>
                        <Text />
                        <Button  style ={{color:'white',float: 'right',    marginTop: -146, height: 43 ,marginRight: 204,backgroundColor: 'white'}} > <img src={aar} alt="aar"/></Button>
                      </Grid>
              </div>
           
        );
    }
}

export default Footer;