import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import classNames from 'classnames';
import Roundimage from '../../../../assets/imgs/round-img.jpg';
import Roundimage2 from '../../../../assets/imgs/round-img2.jpg';
import Roundimage3 from '../../../../assets/imgs/round-img3.jpg';
import Roundimage4 from '../../../../assets/imgs/round-img4.jpg';
import Roundimage5 from '../../../../assets/imgs/round-img5.jpg';
import { Grid } from '@material-ui/core';
import CreateTextElement from '../../common/typography/create_text_element';
import Testimonialtext from '../../../../assets/imgs/test-text.png';




const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const tutorialSteps = [
  {
    label: <div className="text1">
           <p style={{lineHeight: '2.0', textAlign: 'center', color:'white', fontSize:'12px', padding:'5px'}}>Ut a elit diam. Morbi ut mauris eleifend, iaculis mi id, pellentesque mi. Suspendisse gravida tortor ac ante condimentum scelerisque eu eu sem. Suspendisse potenti. Morbi vitae venenatis purus. Nam quis faucibus dui. Curabitur congueeros consequat, in pharetra mi ornare. Aenean non lorem varius leo scelerisque tempor suscipit</p>
           <br/>
                <div style={{textAlign:'center', height:'40px', borderRadius:'360px'}}>
                    <img src={Roundimage} style={{height:'40px', width:'40px', borderRadius:'360px', border:' 2px solid', borderColor:'#0071bc'}}></img>
                </div>
           </div>
  },
  {
    label:  <div className="text2">
            <p style={{lineHeight: '2.0', textAlign: 'center', color:'white', fontSize:'12px'}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, scelerisque eu eu sem. Suspendisse potenti. Morbi vitae venenatis purus. Nam quis faucibus dui. Curabitur congueeros consequat, in pharetra mi ornare. Aenean non</p>
            <br/>
                <div style={{textAlign:'center', height:'40px', borderRadius:'360px'}}>
                    <img src={Roundimage2} style={{height:'40px', width:'40px', borderRadius:'360px', border:' 2px solid', borderColor:'#0071bc'}}></img>
                </div>
            </div>
  },
  {
    label:  <div className="text3">
            <p style={{lineHeight: '2.0', textAlign: 'center', color:'white', fontSize:'12px'}}>Ut a elit diam. Morbi ut mauris eleifend, iaculis mi id, pellentesque mi. Suspendisse gravida tortor ac ante condimentum scelerisque eu eu sem. Suspendisse potenti. Morbi vitae venenatis purus. Nam quis faucibus dui. Curabitur congueeros consequat, in pharetra mi ornare. Aenean non lorem varius leo scelerisque tempor suscipit</p>
            <br/>
                <div style={{textAlign:'center', height:'40px', borderRadius:'360px'}}>
                    <img src={Roundimage3} style={{height:'40px', width:'40px', borderRadius:'360px', border:' 2px solid', borderColor:'#0071bc'}}></img>
                </div>
            </div>
  },
  {
    label:  <div className="text4">
           <p style={{lineHeight: '2.0', textAlign: 'center', color:'white', fontSize:'12px'}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, Suspendisse potenti. Morbi vitae venenatis purus. Nam quis faucibus dui. Curabitur congueeros consequat, in pharetra mi ornare. Aenean non leo scelerisque tempor suscipit</p>
           <br/>
                <div style={{textAlign:'center', height:'40px', borderRadius:'360px'}}>
                    <img src={Roundimage4} style={{height:'40px', width:'40px', borderRadius:'360px', border:' 2px solid', borderColor:'#0071bc'}}></img>
                </div>
            </div>
  },
  {
    label:  <div className="text5">
           <p style={{lineHeight: '2.0', textAlign: 'center', color:'white', fontSize:'12px'}}>Ut a elit diam. Morbi ut mauris eleifend, iaculis mi id, pellentesque mi. Suspendisse gravida tortor ac ante condimentum scelerisque eu eu sem. Suspendisse potenti. Morbi vitae venenatis purus. Nam quis faucibus dui. Curabitur congueeros consequat, in pharetra mi ornare. Aenean non lorem varius leo scelerisque tempor suscipit</p>
           <br/>
                <div style={{textAlign:'center', height:'40px', borderRadius:'360px'}}>
                    <img src={Roundimage5} style={{height:'40px', width:'40px', borderRadius:'360px', border:' 2px solid', borderColor:'#0071bc'}}></img>
                </div>
            </div>
  },
];


const styles = theme => ({
    root: {
      maxWidth: 300,
      flexGrow: 1,
      
    },
    header: {
      display: 'flex',
      alignItems: 'center',
      height: 50,
      paddingLeft: theme.spacing.unit * 4,
     
    },
    customDots:{
      
      flexDirection: 'row',
      
      
       
    },
    custom_dot:{
      backgroundColor: 'darkgray',
      margin: '2px 2px',
      borderRadius: '50%',
      width: '40px',
      height: '40px',
      display: 'none'
    },
    dotActive :{
      background: 'url('+Roundimage+') center center / 100% 100% no-repeat',
      height:'40px',
      width:'40px',
      display: 'none'
    }
  });

class Testimonialss extends React.Component{
    constructor(props){
        super(props)
    }

    state = {
        activeStep: 0,
      };
    
      handleNext = () => {
        this.setState(prevState => ({
          activeStep: prevState.activeStep + 1,
        }));
      };
    
      handleBack = () => {
        this.setState(prevState => ({
          activeStep: prevState.activeStep - 1,
        }));
      };
    
      handleStepChange = activeStep => {
        this.setState({ activeStep });
      };




    backgroundStyle = {
        background: 'url('+this.props.backgroundImage+')',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        minHeight: '450px',
        position: 'relative'
    }

    transLayer = {
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,'+this.props.opacity+')',
        padding: '70px 0'
    }
    text = [
        {heading:'', 
        subheading:'',
        text:'',
        image:''
    }

    ]

    render(){
        const { classes, theme } = this.props;
        const { activeStep } = this.state;
        const maxSteps = tutorialSteps.length;

        return(
            <div style={{...this.backgroundStyle,...this.props.style}}>
                <div style={this.transLayer}>
                    <Grid container >
                        <Grid item md={2}></Grid>
                        <Grid item md={8}>

                        <CreateTextElement
                        element="h5"
                        fontSize= "16px"
                        style={{textAlign:'center'}}
                        >
                            Our Testimonial 
                        </CreateTextElement>

                        <CreateTextElement
                        element="h4"
                        fontSize= "22px"
                        style={{textAlign:'center'}}
                        >
                        What Our Valueable Clients Say <br/> <br/>
                        <img src={Testimonialtext}></img>
                        </CreateTextElement>


                        <Typography style={{color:'white',marginTop: '10', textAlign:'center'}}>{tutorialSteps[activeStep].label}</Typography>
                            <div className="container">
                            <AutoPlaySwipeableViews
                            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                            index={activeStep}
                            onChangeIndex={this.handleStepChange}
                            enableMouseEvents
                            >
                            {tutorialSteps.map((step, index) => (
                                <div key={step.label}>
                                
                                </div>
                            ))}
                            </AutoPlaySwipeableViews>
                            <MobileStepper
                            variant="dots"
                            steps={maxSteps}
                            position="static"
                            activeStep={this.state.activeStep}
                            classes={{dot: classes.custom_dot, dotActive: classes.dotActive ,dots:classes.customDots}}
                            style ={{backgroundColor:'transparent'}}
                            
                            >
                            
                            </MobileStepper>
                            </div>

                        </Grid>
                        <Grid item md={2}></Grid>
                            
                    </Grid>
                </div>
            </div>
        )
    }
}

Testimonialss.defaultProps = {
    opacity: '.50'
}

Testimonialss.propTypes = {
    style: PropTypes.object,
    backgroundImage: PropTypes.string,
    opacity: PropTypes.string,
    classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}

export default withStyles(styles, { withTheme: true })(Testimonialss);