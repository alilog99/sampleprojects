import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid';
import Aboutour from '../AboutComponents/aboutcard/AboutCard';
import CreateTextElement from '../../../typography/create_text_element';
import { AboutOurString } from '../../../../../constants/strings';
import Cards from '../AboutComponents/aboutcard/AbtCards';
import '../AboutComponents/thirdComponent.css';


class ThirdComponent extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>
          
          <Aboutour
      style={{padding: '40px 0'}}
      >
        
        <Grid container spacing={16}>
          {/* <Grid item md={1}></Grid> */}
          <Grid item md={12} xs={12}>

          <CreateTextElement
            element="h5"
            fontSize= "16px"
            style={{textAlign:'center'}}
            >
              About our 
          </CreateTextElement>
          
          <CreateTextElement
            element="h4"
            fontSize= "22px"
            style={{textAlign:'center'}}
            >
            Core Values <br/> 
          
          </CreateTextElement>
          

          <CreateTextElement
            element="p"
            fontSize= "12px"
            style={{lineHeight: '2.0', textAlign: 'center'}}
            >
              {AboutOurString}
              
          </CreateTextElement>
          
          </Grid>
          {/* <Grid item md={1}></Grid> */}
          <Grid item md={4} sm={12} xs={12}>
          
          <Cards>
            <Grid container spacing={16}>
              <Grid item md={4} sm={4} xs={4}>
              <p className="card-logo"><i class="fas fa-cloud-upload-alt" ></i></p>
              </Grid>
              <Grid item md={8} sm={8} xs={8}>
              <p className="card-heading">Cloud Services</p>
              <p  className="card-text">On demand services for an easy & scalable software application</p>
              </Grid>
            
            </Grid>
          </Cards>
          
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
          <Cards>
            <Grid container spacing={16}>
              <Grid item md={4} sm={4} xs={4}>
              <p className="card-logo"><i class="fas fa-tags"></i></p>
              </Grid>
              <Grid item md={8} sm={8} xs={8}>
              <p className="card-heading">E-Commerce Build</p>
              <p className="card-text">Online Product/Service Trading Systems with end to end technology support</p>
              </Grid>
            
            </Grid>
          </Cards>
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
          <Cards>
            <Grid container spacing={16}>
              <Grid item md={4} sm={4} xs={4}>
              <p className="card-logo"><i class="far fa-credit-card"></i></p>
              </Grid>
              <Grid item md={8} sm={8} xs={8}>
              <p className="card-heading">Smart Card</p>
              <p className="card-text">Secure ID in compliance to international standards contact & contactless</p>
              </Grid>
            
            </Grid>
          </Cards>
          </Grid>
          <Grid item md={2}></Grid>
          <Grid item md={4} sm={12} xs={12}>
          <Cards>
            <Grid container spacing={16}>
              <Grid item md={4} sm={4} xs={4}>
              <p className="card-logo"><i class="fas fa-chalkboard-teacher"></i></p>
              </Grid>
              <Grid item md={8} sm={8} xs={8}>
              <p className="card-heading">E-Learning</p>
              <p className="card-text">Learning Management Systems to enable user to take up the courses & learn at their ease</p>
              </Grid>
            
            </Grid>
          </Cards>
          </Grid>
          <Grid item md={4} sm={12} xs={12}>
          <Cards>
            <Grid container spacing={16}>
              <Grid item md={4} sm={4} xs={4}>
              <p className="card-logo"><i class="far fa-calendar-alt"></i></p>
              </Grid>
              <Grid item md={8} sm={8} xs={8}>
              <p className="card-heading">Event</p>
              <p className="card-text">Enjoy hastle free management of your Event with us right from ticketing</p>
              </Grid>
            
            </Grid>
          </Cards>
          </Grid>
      </Grid>
      

    </Aboutour>
            </div>
        )
    }
}

ThirdComponent.defaultProps = {

}

ThirdComponent.propTypes = {
    style: PropTypes.object
}

export default ThirdComponent;