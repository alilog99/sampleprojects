import React from 'react'
import PropTypes from 'prop-types'
import BrandContainer from '../AboutComponents/AboutBrand_Container';
import Grid from '@material-ui/core/Grid';
import  Companies from '../../../../../../assets/imgs/polizia.png';
import  Myriad from '../../../../../../assets/imgs/myriad.png';
import  Film from '../../../../../../assets/imgs/fujifilm.png';
import  Sain from '../../../../../../assets/imgs/sainsbury.png';
import  Cain from '../../../../../../assets/imgs/McCain.png';
import  Honey from '../../../../../../assets/imgs/honeywell.png';

class AboutBrand extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>
               <BrandContainer
    
               style={{padding: '10px 0',minHeight: '10px'}}
                 >
                <Grid>
                 <h6 style={{textAlign:'center',color: 'gray',marginBottom: '-20px',marginTop: '32px'}}>Working with some great Brand</h6>
                 <div style={{marginLeft:'80px',verticalAlign:'middle'}}/>
                 <img style={{width: '80px',margin:'15px',marginLeft:'40px'}} src={Companies} alt="companies"/>
                 <img  style={{width: '150px',margin:'15px',marginLeft:'20px'}} src={Myriad} alt="myriad"/>
                 <img  style={{width: '150px',margin:'15px',marginLeft:'20px'}}src={Film} alt="film"/>
                 <img  style={{width: '150px',margin:'15px',marginLeft:'20px'}}src={Sain} alt="sainsbury"/>
                 <img  style={{width: '150px',margin:'15px',marginLeft:'20px'}}src={Cain} alt="mccain"/>
                 <img  style={{width: '200px',margin:'15px',marginLeft:'20px'}}src={Honey} alt="mccain"/>
                </Grid>    
               </BrandContainer> 
            </div>
        )
    }
}

AboutBrand.defaultProps = {

}

AboutBrand.propTypes = {
    style: PropTypes.object
}

export default AboutBrand;