import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
// import Woman from '../assets/contact-us/woman.png';
// import Background from '../assets/contact-us/backgroud.png';
// import Rectangle from '../assets/contact-us/rectangle.png';
// import Cover from '../assets/contact-us/cover.png';
import '../contact us/contact.css';
// import {Link , Route} from 'react-router-dom';
// import Navbar from '../components/CustomNavbar';
import 'react-bootstrap';
import FooterCompo from '../Home page componentts/FooterCompo';
import AboutBrand from '../About Us/AboutComponents/aboutbrand';
import TextboxFooter from './footer text/textbox';
import TextboxFooter2 from './footer text/textbox2';
import { TextField } from '@material-ui/core';
import Button1 from '../../button/button';
import ar from '../../../../../assets/imgs/arrowblack.png'
import Navbar from '../../Navbar/navbar';
import OurServ1 from './ContactUs1'


class Contact extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>
                
                <OurServ1/>
                <Grid container>
                    <Grid  item xs={12} sm={12} md={12} lg={12}>
                        <div  className="background1-size">
                        <div className='background2-size'></div>
                        <Grid container>
                            <Grid item xs={2} sm={2} md={2} lg={2}></Grid>
                            <Grid item xs={0} sm={0} md={0} lg={0}></Grid>
                            <Grid item xs={1} sm={1} md={1} lg={1} style={{marginLeft:'-15px'}}>
                                <div className='address-logo' ></div>
                            </Grid>
                            <Grid item xs={1} sm={1} md={1} lg={1}></Grid>
                            <Grid item xs={2} sm={2} md={2} lg={2}></Grid>
                            <Grid item xs={2} sm={1} md={1} lg={1}>
                                <div className='phone-logo'></div>
                            </Grid>

                            <Grid item xs={1} sm={2} md={2} lg={1}></Grid>
                            <Grid item xs={1} sm={1} md={1} lg={2}></Grid>
                            <Grid item xs={2} sm={1} md={1} lg={1}>
                                <div className='email-logo'></div>
                                
                            </Grid>
                            
                        </Grid>
                        <Grid container>
                            <Grid item xs={1} sm={0} md={0} lg={0}></Grid>
                            <Grid item xs={0} sm={0} md={0} lg={0}></Grid>
                            <Grid item xs={2} sm={2} md={2} lg={2}>
                            <p className='font'>Address</p>
                            <p className='font2'>282 Horton Road Datchet SL39HN (UK)47096</p>
                            </Grid>
                            <Grid item xs={0} sm={0} md={1} lg={1}></Grid>
                            <Grid item xs={1} sm={2} md={1} lg={1}></Grid>
                            <Grid item xs={3} sm={2} md={2} lg={2} style={{marginLeft:'15px'}}>
                            <p className='font'>Phone</p>
                            <p className='font2'>(0044) 7968 670</p>
                            </Grid>
                            <Grid item xs={0} sm={1} md={1} lg={1}></Grid>
                            <Grid item xs={1} sm={1} md={1} lg={1}></Grid>
                            <Grid item xs={3} sm={2} md={2} lg={2}>
                            <p className='font' style={{marginLeft:'-15px'}}>Email</p>
                            <p className='font2'>info@rndtek.co.uk.com</p>
                            </Grid>
                            
                            <Grid item lg={7} md={7} xs={12}>
                            <div className='contact-box'>
                                <Grid container>
                                    <Grid item xs={1} md={2} lg={2}></Grid>
                                    <Grid item xs={11} md={10} lg={10}>
                                    <h5 style={{color:'#fff', fontSize:'27px', marginTop:'20px'}}>Get in Touch</h5>
                                    <p style={{color: '#fff', fontSize:'12px'}}>CONTACT US</p>

                                    </Grid>
                                    <Grid item md={2}></Grid>
                                    <Grid item xs={12} md={3}>
                                    <TextboxFooter/>
                                    </Grid>
                                    <Grid item md={1}></Grid>
                                    
                                    
                                    <Grid item xs={12} md={3}>
                                    <TextboxFooter2/>
                                    </Grid>
                                    <Grid item  md={3}></Grid>
                                    <Grid item  md={2}></Grid>
                                    <Grid item xs={12} md={7}>
                                    
                                    <textarea rows={4} placeholder="Your message" style={{width:'100%', padding:'10px'}}></textarea>
                                    
                                    </Grid>
                                    <Grid item  md={3}></Grid>
                                    <Grid item xs={3}  md={2}></Grid>
                                    <Grid item xs={9} md={4}>
                                    <Button1 
                                    style={{backgroundColor:'#fff', borderRadius:'25px', marginTop:'10px'}}
                                    Gname= "SEND MESSAGE "
                                    imgarr={ar}
                                        />
                                    </Grid>
                                            {/* <Grid container>
                                                <Grid item xs={2}></Grid>
                                                <Grid item xs={8}>
                                                
                                                <p style={{color:'white'}}>Contact us</p>
                                                <textarea style={{height:'35px', width:'200px'}} placeholder='Your name'></textarea>
                                                <textarea style={{height:'35px', width:'200px', marginLeft:'20px'}} placeholder='Your email'></textarea>
                                                <textarea style={{height:'90px', width:'420px', marginTop:'20px'}} placeholder='Your message'></textarea><br />
                                                <button>SEND MESSAGE</button>
                                                </Grid>
                                            </Grid> */}
                                        
                                </Grid>
                            </div>
                            </Grid>
                            <Grid item lg={3} md={3}>
                                <div className='contact-woman'></div>
                            </Grid>
                            <Grid item xs={1}></Grid>
                            {/* <Grid item lg={4} xs={0}>
                            <div className='contact-image'>
                            </div>
                            <div className='contact-woman'></div>
                            </Grid> */}
                            
                            
                        </Grid>
                        
                            
                        
                        
                        </div>
                    </Grid>
                </Grid>
            <AboutBrand />
            <FooterCompo />
            </div>
        )
    }
}

Contact.defaultProps = {

}


export default Contact;








{/* <div>
              <img src={Cover} alt='cover' style={{width:'100%', height:'300px'}}></img>;
              <img src={Rectangle} alt='rectangle' style={{marginTop:'-25px'}}></img>
              <img src={Background} alt='background' style={{position:'absolute', marginTop:'-1500px'}}></img>
              <div style={{color:'white', height:'400px', width:'800px', backgroundColor:'#0071bc', marginTop:'-800px',marginLeft:'0px', position:'absolute'}}>
              <h1 style={{marginLeft:'200px'}}>Get in touch</h1>
              <p style={{marginLeft:'200px'}}>Contact us</p>
              <textarea style={{marginLeft:'200px'}}></textarea><textarea style={{marginLeft:'10px'}}></textarea> <br />
              <textarea style={{height:'100px', marginLeft:'200px' , width:'330px' , marginTop:'20px'}}></textarea>
              </div>
              <div style={{height:'350px',width:'400px',position:'absolute', marginTop:'-750px', marginLeft:'700px'}}>
              <img src={Image} alt='image' style={{position:'absolute', marginTop:'0px', height:'300px',width:'400px'}}></img>
              </div>
              <img src={Woman} alt='image' style={{position:'absolute', marginTop:'-750px', height:'300px',width:'250px', marginLeft:'650px'}}></img>
              <img src={Address} style={{position:'absolute', marginTop:'-920px', marginLeft:'320px'}}></img>
              <p style={{position:'absolute', marginTop:'-870px', marginLeft:'300px', color:'white'}}>Address</p>
              <p style={{position:'absolute', marginTop:'-840px', marginLeft:'220px', color:'white'}}>282 Horton Road Datchet SL39HN</p>
              <img src={Phone} style={{position:'absolute', marginTop:'-920px', marginLeft:'650px'}}></img>
              <p style={{position:'absolute', marginTop:'-870px', marginLeft:'650px', color:'white'}}>Phone</p>
              <p style={{position:'absolute', marginTop:'-840px', marginLeft:'620px', color:'white'}}>(0044) 7968 670</p>
              <img src={Mail} style={{position:'absolute', marginTop:'-920px', marginLeft:'1000px'}}></img>
              <p style={{position:'absolute', marginTop:'-870px', marginLeft:'1000px', color:'white'}}>Email</p>
              <p style={{position:'absolute', marginTop:'-840px', marginLeft:'930px', color:'white'}}>info@rndtek.co.ukmail.com</p>
              <Header changeTitle={this.changeTitle.bind(this)} title={this.state.title} />
              
            </div> */}