import React from 'react'
import PropTypes from 'prop-types'

class AboutBackTop extends React.Component{
    constructor(props){
        super(props)

    }

    backgroundStyle = {
        background: 'url('+this.props.backgroundImage+')',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        minHeight: '500px',
        position: 'relative',
        
    }

    

    render(){
        return(
            <div style={{...this.backgroundStyle,...this.props.style}}>
            <div className="container" style={{position:'relative'}}>
            
                {this.props.children}
            </div>
        </div>
        )
    }
}

AboutBackTop.defaultProps = {
    opacity: '.50'
}

AboutBackTop.propTypes = {
    style: PropTypes.object,
    backgroundImage: PropTypes.string,
    opacity: PropTypes.string
}

export default AboutBackTop;