import React from 'react'
import PropTypes from 'prop-types'
import about from '../../../../../assets/imgs/cover.png';
import AboutBackTop from '../contact us/firstcompon';
import Nav from '../../Navbar/navbar';
import Grid from '@material-ui/core/Grid';
import Btn from '../../button/button';
import ar from '../../../../../assets/imgs/arrow.png';

class OurServ1 extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>
              <AboutBackTop  backgroundImage ={about}
    
                 style={{paddingBottom: '150px'}}
              >
                <Grid container spacing={16}>
                  <Grid item  md ={12} lg={12} xs={12} >
                           <Nav/>
                 </Grid>
                      
                 </Grid>

             </AboutBackTop>
            </div>
        )
    }
}

OurServ1.defaultProps = {

}

OurServ1.propTypes = {
    style: PropTypes.object
}

export default OurServ1;