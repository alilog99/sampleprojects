import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


const styles = theme => ({
    root: {
      width: '100%',
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
  });


class QuestionAchieve extends React.Component{
    constructor(props){
        super(props)
    }

    state = {
        expanded: null,
      };
    
      handleChange = panel => (event, expanded) => {
        this.setState({
          expanded: expanded ? panel : false,
        });
      };

    backgroundStyle = {
        backgroundColor: '#051320',
        minHeight: '400px',
        position: 'relative'
    }
    
    render(){
        const { classes } = this.props;
        const { expanded } = this.state;
        return(
            
            <div className="row" >
                <div className="col-12 col-xs-12 col-sm-12 col-md-6">
                    <div style={{...this.props.style}}>
                        <div className="container">
                            <div className="col-12 col-xs-12 col-sm-12 col-md-12" style={{paddingRight:'110px', marginLeft:'80px',marginTop:'70px', marginBottom:'40px'}}>
                                <h5 style={{color:'#0071bc', fontSize:'27px',marginTop:'20px'}}>Frequently Asked Question</h5>
                                <div className={classes.root} style={{marginTop:'30px'}}>
                                <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange('panel1')}>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                   <h4 style={{fontSize:'16px'}}>How to Transition From Finance Into Consulting?</h4>
                                    
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <Typography>
                                    Nulla facilisi. Phasellus sollicitudin nulla et quam mattis feugiat. Aliquam eget
                                    maximus est, id dignissim quam.
                                    </Typography>
                                </ExpansionPanelDetails>
                                </ExpansionPanel>
                                <ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handleChange('panel2')}>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                <h4 style={{fontSize:'16px'}}>How Important is GPA for MBA Consulting?</h4>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <Typography>
                                    Donec placerat, lectus sed mattis semper, neque lectus feugiat lectus, varius pulvinar
                                    diam eros in elit. Pellentesque convallis laoreet laoreet.
                                    </Typography>
                                </ExpansionPanelDetails>
                                </ExpansionPanel>
                                <ExpansionPanel expanded={expanded === 'panel3'} onChange={this.handleChange('panel3')}>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                <h4 style={{fontSize:'16px'}}>I Know Nothing About Consulting</h4>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <Typography>
                                    Nunc vitae orci ultricies, auctor nunc in, volutpat nisl. Integer sit amet egestas
                                    eros, vitae egestas augue. Duis vel est augue.
                                    </Typography>
                                </ExpansionPanelDetails>
                                </ExpansionPanel>
                                
                            </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div className="col-12 col-xs-12 col-sm-12 col-md-6">
                    <div style={{...this.backgroundStyle,...this.props.style}}>
                        
                        <div className="container">
                                
                                
                            <div className="row">
                            <div className="col-12 col-xs-12 col-md-12">
                                <h5 style={{color:'#fff', fontSize:'27px', marginTop:'70px', marginLeft:'15px'}}>Our Achievement</h5>
                            </div>
                            <div className="col-12 col-sm-12 col-xs-12 col-md-6" style={{marginTop:'20px'}}>
                                <div className="row">
                                    <div className="col-2 col-xs-2 col-sm-2 col-md-2">
                                    <p style={{color:'white',fontSize:'40px', textAlign:'center'}}>
                                    <i class="fab fa-react"></i>
                                    </p>
                                    </div>
                                    <div className="col-10 col-xs-10 col-sm-10 col-md-10">
                                    <h5 style={{fontSize:'27px', color:'#0071bc'}}>15</h5>
                                    <h5 style={{color:'#fff', fontSize:'17px'}}>Years Of Experience</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-xs-12 col-md-6" style={{marginTop:'20px'}}>
                                <div className="row">
                                    <div className="col-2 col-xs-2 col-sm-2 col-md-2">
                                        <p style={{color:'white',fontSize:'40px'}}>
                                        <i class="far fa-thumbs-up"></i>
                                        </p>
                                    </div>
                                    <div className="col-10 col-xs-10 col-sm-10 col-md-10">
                                        <h5 style={{fontSize:'27px', color:'#0071bc', marginLeft:'0px'}}>75</h5>
                                        <h5 style={{color:'#fff', fontSize:'17px', marginLeft:'0px'}}>Happy Clients</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-xs-12 col-md-6" style={{marginTop:'20px'}}>
                                <div className="row">
                                    <div className="col-2 col-xs-2 col-sm-2 col-md-2">
                                    <p style={{color:'white',fontSize:'40px'}}>
                                    <i class="fas fa-briefcase"></i>
                                    </p>
                                    </div>
                                    <div className="col-10 col-xs-10 col-sm-10 col-md-10">
                                    <h5 style={{fontSize:'27px', color:'#0071bc'}}>120+</h5>
                                    <h5 style={{color:'#fff', fontSize:'17px'}}>Complete Projects</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-xs-12 col-md-6" style={{marginTop:'20px'}}>
                                <div className="row">
                                    <div className="col-2 col-xs-2 col-sm-2 col-md-2">
                                    <p style={{color:'white',fontSize:'40px'}}>
                                    <i class="fas fa-dollar-sign"></i>
                                    </p>
                                    </div>
                                    <div className="col-10 col-xs-10 col-sm-10 col-md-10">
                                    <h5 style={{fontSize:'27px', color:'#0071bc', marginLeft:'0px'}}>150K+</h5>
                                    <h5 style={{color:'#fff', fontSize:'17px', marginLeft:'0px'}}>Money Saved</h5>
                                    </div>
                                </div>
                            </div>

                            </div>
                        </div>
                    
                        
                    </div>
                </div>
            </div>
        )
    }
}

QuestionAchieve.defaultProps = {
    opacity: '.50'
}

QuestionAchieve.propTypes = {
    style: PropTypes.object,
    backgroundImage: PropTypes.string,
    opacity: PropTypes.string,
    classes: PropTypes.object.isRequired
}

export default withStyles(styles) (QuestionAchieve);