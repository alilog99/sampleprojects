import React from 'react'
import PropTypes from 'prop-types'

import CreateTextElement from '../../typography/create_text_element';
class PortGridContainer extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{display: 'inline-block',...this.props.style, margin: '16px'}}>
                <img style={{width: '300px'}} src={this.props.backImg}  />

                <CreateTextElement
                    element="div"
                    color= 'black'
                    style={{textAlign:'center'}}
                    >
                    {this.props.title}
                    
                    
                
                </CreateTextElement>
                    
                <CreateTextElement
                    element="div"
                    color= 'blue'
                    style={{textAlign:'center'}}
                    >
                    {this.props.name }
                </CreateTextElement>
            </div>

            
        )
    }
}

PortGridContainer.defaultProps = {

}

PortGridContainer.propTypes = {
    style: PropTypes.object
}

export default PortGridContainer;