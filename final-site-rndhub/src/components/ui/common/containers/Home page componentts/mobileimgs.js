import React from 'react'
import PropTypes from 'prop-types'
import MobileContainer from '../Home page componentts/mobileimgcompo';
import Grid from '@material-ui/core/Grid';
import Aboutbrand from '../About Us/AboutComponents/aboutbrand';
import Mobimg1 from '../../../../../assets/imgs/mobimg1.png';
import Mobimg2 from '../../../../../assets/imgs/mobimg2.png';
import Mobimg3 from '../../../../../assets/imgs/mobimg3.png';
import Mobimg4 from '../../../../../assets/imgs/mobimg4.png';

class MobileImg extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div >
           <Aboutbrand/>
    


     <MobileContainer 
      
        style={{margintop: '80px'}}
     >
      <Grid container spacing={14}>
              <Grid item xs={12} lg={6} md={6}  style={{marginTop: '0px'}} >
                <Grid item xs={12} lg={12} md={12} >
                <img src={Mobimg1} alt="aboutmobo" style={{width:'100%', height:'519px'}} />
            
              </Grid>
              </Grid>
              <Grid item xs={12} md={6} lg={6} >  
                
                  <img src={Mobimg2} alt="aboutmobo" style={{width:'100%', height:'519px'}} />
              
              </Grid>
            
            </Grid>
            
    </MobileContainer> 

<MobileContainer 

>
<Grid container spacing={14} style={{width:'100%'}}>
              <Grid item xs={12} lg={6} md={6}  style={{marginTop: '0px'}} >
                <Grid item xs={12} lg={12} md={12} >
                <img src={Mobimg3} alt="aboutmobo" style={{width:'100%', height:'519px',display:'block'}} />
            
              </Grid>
              </Grid>
              <Grid item xs={12} md={6} lg={6} >  
                
                  <img src={Mobimg4} alt="aboutmobo" style={{width:'100%', height:'519px'}} />
              
              </Grid>
            
            </Grid>

</MobileContainer>

            </div>
        )
    }
}

MobileImg.defaultProps = {

}

MobileImg.propTypes = {
    style: PropTypes.object
}

export default MobileImg;