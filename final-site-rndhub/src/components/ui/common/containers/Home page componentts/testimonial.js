import React from 'react'
import PropTypes from 'prop-types'

class Testimonial extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>

            </div>
        )
    }
}

Testimonial.defaultProps = {

}

Testimonial.propTypes = {
    style: PropTypes.object
}

export default Testimonial;