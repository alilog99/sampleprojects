import React from 'react'
import PropTypes from 'prop-types'
import {Link , Route} from 'react-router-dom';
import Card from '../../../etc/Card';
import App from '../../../../../assets/imgs/app_dev.png';
import web from '../../../../../assets/imgs/web_dev.png';
import digi from '../../../../../assets/imgs/digital_mon.png';
import game from '../../../../../assets/imgs/gamedevicon.png';
import vrr from '../../../../../assets/imgs/vrappicon.png';
import dig from '../../../../../assets/imgs/digmarkicon.png';
import Button1 from '../../button/button';
import { bannerString } from '../../../../constants/strings';
import bannerImg from '../../../../../assets/imgs/banner.png';
import BackgroundContainer from '../Home page componentts/background_container';
import CreateTextElement from '../../typography/create_text_element';
import Grid from '@material-ui/core/Grid';
import background1 from '../../../../../assets/imgs/Layer20.png';
import Slide from '../../../etc/text carosal/slider';
import ar from '../../../../../assets/imgs/arrow.png';
import Nav from '../../Navbar/navbar';
import TopBackground from '../Home page componentts/top_backgroundcont';
import Btn from '../../button/button';
import Footer from '../Home page componentts/footer';
import TextboxFooter from '../../../etc/footer text/textbox';
import logo from '../../../../../assets/imgs/rnd_logo.png';
import { footerString } from '../../../../constants/strings';
import Testimonials from '../../../etc/testimonial/Testimonial';
import  MobileImg from '../Home page componentts/mobileimgs'; 
import Home3Com from '../Home page componentts/Home3Com';


class Background1 extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
          <div>
          <TopBackground   backgroundImage = {background1}
   
          style={{paddingBottom: '150px'}}
          > 
          <Grid container spacing={16}>
              <Grid item  md ={12} lg={12} xs={12} >
           
                 <Nav  />
                <Slide  /> 
                <Btn  Gname="Get Started" imgarr={ar} style={{position: 'relative',minHeight: '100px',maxHeight: '123px',color:'white', backgroundColor:'#0071bc' , minHeight: '59px', width:'186px', borderRadius: '43px',marginTop: '4px',marginLeft: '125px'}} > </Btn>
              </Grid>
           </Grid>
          </TopBackground>
            <BackgroundContainer
            backgroundImage = {bannerImg}
            opacity = ".95"
            
            > 
      
            
            <Grid container spacing={16}>
              <Grid item xs={6}  style={{marginTop: '92px'}} >
                <Grid item xs={12} lg={12} md={12} >
      
              <CreateTextElement
                element="h5"
                fontSize= "20px"
                >
                  Thriving To Achieve Smart, 
              </CreateTextElement>
              
              <CreateTextElement
                element="h4"
                fontSize= "30px"
                >
                  Unique & State Of The Art Solutions
              </CreateTextElement>
      
              <CreateTextElement
                element="p"
                fontSize= "15px"
                
                style={{lineHeight: '2.1'}}
                >
                  {bannerString}
              </CreateTextElement>

              <Button1 Gname="Get Started" imgarr={ar}style={{position: 'absalute',minHeight: '100px',maxHeight: '123px',color:'white', backgroundColor:'#0071bc' , minHeight: '59px', width:'186px', borderRadius: '43px',marginLeft: '1px'}} />
             
              </Grid>
              </Grid>
              <Grid item xs={6}  style={{marginTop: '92px'}}>  
                 <Grid item  lg={12} md={12}>
                <Card   Imgsd={App} Tname="app development"   />
                <Card   Imgsd={web} Tname="Web development"/>
                <Card   Imgsd={digi} Tname="Digital marketing"/>
                <Card   Imgsd={game} Tname="game development"/>
                <Card   Imgsd={vrr} Tname="VR Application"/>
                <Card   Imgsd={dig} Tname="Digital Marketing"/>
                </Grid>
              </Grid>
              
             
            </Grid>
            
      
          </BackgroundContainer>
          <Home3Com />
          <MobileImg />
          <Testimonials ></Testimonials>
          <Footer
      style={{padding: '50px 0'}}
      >
        
      <div className="container">
        <div className="row">
          <div className="col-12 col-sm-6 col-md-3">
            <img src={logo} alt=""/>
            
            <CreateTextElement
              element="p"
              fontSize= "12px"
              style={{lineHeight: '2.1', marginTop:'10px'}}
              >
                {footerString} 
            </CreateTextElement>
          </div>

          <div className="col-12 col-sm-6 col-md-3">
            <CreateTextElement
              element="h5"
              fontSize= "16px"
              style={{padding: '20px 5px 8px 5px'}}
              >
                Support
                
            </CreateTextElement>

            <CreateTextElement
            element='p'
            fontSize='13px'
            >
            <ul style={{listStyle:'none', padding: '0px 10px', lineHeight: '2.1'}}>
                  <li><a href="#">Help Center</a></li>
                  <li><a href="#">Get Started</a></li>
                  <li><a href="#">Contact US</a></li>
            </ul>
            </CreateTextElement>

          </div>

          <div className="col-12 col-sm-6 col-md-3">
            <CreateTextElement
              element="h5"
              fontSize= "16px"
              style={{padding: '20px 5px 8px 5px'}}
              >
                About US
                
            </CreateTextElement>

            <CreateTextElement
            element='p'
            fontSize='13px'
            >
            <ul style={{listStyle:'none', padding: '0px 10px', lineHeight: '2.1'}}>
                   <li><a href="#">About US</a></li>
                  <li><a href="#">Terms of Use</a></li>
                  <li><a href="#">Privacy Policy</a></li>
            </ul>
            </CreateTextElement>
          
          </div>

          <div className="col-12 col-sm-6 col-md-3">
            <CreateTextElement
              element="h5"
              fontSize= "16px"
              style={{padding: '20px 5px 8px 5px'}}
              >
                Get Newsletter
                
            </CreateTextElement>

            <TextboxFooter 
            style={{padding:'5px 0px 0px 10px'}}
            />
            <CreateTextElement
              
            >
            <i className="fas fa-globe" style={{padding:'0px 5px 10px 23px'}}></i>
            <i className="fab fa-facebook-f" style={{padding:'0px 20px'}}></i>
            <i className="fab fa-google-plus-g" style={{padding:'0px 8px'}}></i>
            <i className="fab fa-twitter" style={{padding:'0px 8px'}}></i>
            
            </CreateTextElement>
          </div> 
        </div>
        </div>
      

    </Footer>
          </div>
        )
    }
}

Background1.defaultProps = {

}

Background1.propTypes = {
    style: PropTypes.object
}

export default Background1;