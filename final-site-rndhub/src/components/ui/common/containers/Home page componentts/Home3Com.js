import React from 'react'
import PropTypes from 'prop-types'
import HomeThirdCompo from '../Home page componentts/HomeThirdCompo';
import ar from '../../../../../assets/imgs/arrowblack.png';
import { abouthome } from '../../../../constants/strings';
import aboutmobo from '../../../../../assets/imgs/aboutmobo.png';
import CreateTextElement from '../../typography/create_text_element';

import Grid from '@material-ui/core/Grid';
import Button1 from '../../button/button';


class Home3Com extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <div style={{...this.props.style}}>
              <HomeThirdCompo
                style={{marginTop: '7px'}}
            opacity = ".85"
            
            > 
      
            
            <Grid container spacing={12}>
              <Grid item xs={12} lg={6} md={6}  style={{marginTop: '92px'}} >
                <Grid item xs={12} lg={12} md={12} >
                <h1 style={{color:'   #ffffff25', fontSize: '61px',marginLeft:'119px'}}><strong>About us</strong></h1>

              <CreateTextElement
                element="h5"
                fontSize= "16px"
                style={{ color: "white",marginTop: "-32px"}}
                marginLeft="120px"
                >
                  You have got an idea?
              </CreateTextElement>
              
              <CreateTextElement
                element="h4"
                fontSize= "30px"
                marginLeft="117px"
                >
                  We can turn it into reality 
              </CreateTextElement>
      
              <CreateTextElement
                element="p"
                fontSize= "15px"
                marginLeft="116px"
                style={{lineHeight: '2.1',marginRight:"34px"}}
                >
                  {abouthome}
              </CreateTextElement>
              <Button1 Gname="Get Started" imgarr={ar}style={{position: 'absalute',minHeight: '100px',maxHeight: '123px',color:'black', backgroundColor:'white' , minHeight: '59px', width:'186px', borderRadius: '43px',marginTop: '-8px',marginLeft:'110px',    marginBottom: '22px'}} />
            
              </Grid>
              </Grid>
              <Grid item xs={12} md={6} lg={6} >  
                
                  <img src={aboutmobo} alt="aboutmobo" style={{width:'100%', height:'519px'}} />
              
              </Grid>
            
            </Grid>
            
      
          </HomeThirdCompo>
            </div>
        )
    }
}

Home3Com.defaultProps = {

}

Home3Com.propTypes = {
    style: PropTypes.object
}

export default Home3Com;