import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Route} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.css';
import Backgound1 from '../src/components/ui/common/containers/Home page componentts/background';
import Aboutus from '../src/components/ui/common/containers/About Us/aboutuspge';
import Portfolio from '../src/components/ui/common/containers/Portfolio/Portfoliopage';
import Contactus from '../src/components/ui/common/containers/contact us/contact-us';
import OurServices from '../src/components/ui/common/containers/our-services/services';
require('../src/assets/css/main.css')

class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div >
      <Route exact={true} path='/background' render={() => (
            <div className="App">
            <Backgound1 />

            </div>
          )}/>
            <Route exact={true} path='/aboutuspage' render={() => (
            <div className="App">
            <Aboutus/>

            </div>
          )}/>
            <Route exact={true} path='/portfoliopage' render={() => (
            <div className="App">
            <Portfolio />

            </div>
          )}/>
           <Route exact={true} path='/contact-us' render={() => (
            <div className="App">
            <Contactus />

            </div>
          )}/>
          <Route exact={true} path='/Services' render={() => (
            <div className="App">
            <OurServices/>

            </div>
          )}/>
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
